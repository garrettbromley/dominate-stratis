private["_mode", "_spawnPoints", "_className", "_cbh9q", "_colorIndex", "_spawnPoint", "_vehicle"];

if((lbCurSel 2302) == -1) exitWith {hint "You did not pick a vehicle!"};

_className = lbData[2302,(lbCurSel 2302)];
_vIndex = lbValue[2302,(lbCurSel 2302)];
_vehiclePrice = (vehicleList select _vIndex) select 1;
_colorIndex = lbValue[2304,(lbCurSel 2304)];
_currentStore = player getVariable "currentStore";
_storePoints = _currentStore getVariable "availablePoints";
_playerPoints = player getVariable "forcePoints";
_playerPointsSpent = player getVariable "forcePointsSpent";

if(_vehiclePrice < 0) exitWith {};
if((_storePoints + _playerPoints) < _vehiclePrice) exitWith {
	hint format["You do not have enough cash to purchase this vehicle.\n\nAmount Lacking: %1 Force Points", [_vehiclePrice - (_storePoints + _playerPoints)] call fnc_numberText];
};

_spawnPoints = player getVariable "currentSpawnPoint";
_spawnPoint = "blocked";

if (count(nearestObjects[(getMarkerPos _spawnPoints), ["Car", "Ship", "Air"], 5]) == 0) then {
	_spawnPoint = "clear"
};

if (_spawnPoint == "blocked") exitWith {
	hint "There is a vehicle currently blocking the spawn point";
};

if (_storePoints < _vehiclePrice) then {
	_price = _vehiclePrice - _storePoints;
	_storePoints = 0;
	_playerPoints = _playerPoints - _price;
} else {
	_storePoints = _storePoints - _vehiclePrice;
};

_currentStore setVariable ["availablePoints", _storePoints, true];
player setVariable ["forcePoints", _playerPoints, true];

hint format ["You requested a %1 for %2 Force Points", getText(configFile >> "CfgVehicles" >> _className >> "displayName"),[_vehiclePrice] call fnc_numberText];
_vehicle = createVehicle [_className, (getMarkerPos _spawnPoints), [], 0, "NONE"];

waitUntil {!isNil "_vehicle"};

_vehicle allowDamage false;
_vehicle setVectorUp (surfaceNormal (getMarkerPos _spawnPoints));
_vehicle setDir (markerDir _spawnPoints);
_vehicle setPos (getMarkerPos _spawnPoints);
_vehicle disableTIEquipment true;
_vehicle allowDamage true;
_vehicle addEventHandler ["Killed", {[_this select 1, 300] execVM "scripts\serverFunctions\vehicleKilled.sqf";}];

closeDialog 0;

player moveInDriver _vehicle;

player setVariable ["currentSpawnPoint", "", true];
player setVariable ["currentStore", nil, true];
_playerPointsSpent = _playerPointsSpent + _vehiclePrice;
player setVariable ["forcePointsSpent", _playerPointsSpent, true];
