private ["_pos", "_rndm"];
_pos  = ["range", "killFarm", "baldy", "rogaine", "mike26", "maxwell", "tempest"];
_rndm = _pos select floor random count _pos;
[getMarkerPos _rndm, "BLUFOR and CSAT forces fight for control over Stratis.", 20, 50] spawn BIS_fnc_establishingShot;

#define MUSIC_LIST "LeadTrack01_F_Heli", "LeadTrack01_F_EPA", "LeadTrack01_F_EPC", "EventTrack03_F_EPC", "LeadTrack02_F_EXP", "LeadTrack01b_F_EXP", "LeadTrack04_F_EXP", "AmbientTrack02_F_EXP"
[[MUSIC_LIST] call BIS_fnc_selectRandom] call playerFunctions_fnc_playMusic;

waitUntil {alive player};
waitUntil {player == player};

aliveFrom = time;

Storm_fnc_checkPlayer = compile preprocessFile "database\checkPlayer.sqf";
Storm_fnc_getPlayerData = compile preprocessFile "database\getPlayerData.sqf";
Storm_fnc_setPlayerData = compile preprocessFile "database\setPlayerData.sqf";
Storm_fnc_insertPlayer = compile preprocessFile "database\insertPlayer.sqf";
Storm_fnc_getPlayerGear = compile preprocessFile "database\getPlayerGear.sqf";
Storm_fnc_setPlayerGear = compile preprocessFile "database\setPlayerGear.sqf";

player addEventHandler ["Killed", {Null = [_this select 0, _this select 1, _this select 2] execVM "scripts\serverFunctions\playerKilled.sqf";}];
player addEventHandler ["Respawn", {[] call playerFunctions_fnc_assignGear;}];
player addEventHandler ["InventoryOpened", {[_this] call playerFunctions_fnc_openPack;}];
player setVariable ["Backpack_Allow_Access", false, true];
player setVariable ["playerExists", nil, true];
player setVariable ["playerXP", 0, true];
player setVariable ["playerRankNo", 0, true];
player setVariable ["playerRankName", nil, true];
player setVariable ["playerRankIcon", nil, true];
player setVariable ["playerGear", [], true];
player setVariable ["playerNews", [], true];
player setVariable ["aliveTime", 0, true];

player setVariable ["playerKills", 0, true];
player setVariable ["friendlyKills", 0, true];
player setVariable ["totalPlayerKills", 0, true];
player setVariable ["totalFriendlyKills", 0, true];
player setVariable ["totalAIKills", 0, true];
player setVariable ["vehicleKills", 0, true];
player setVariable ["playerDeaths", 0, true];
player setVariable ["deathsFromPlayers", 0, true];
player setVariable ["deathsFromAI", 0, true];
player setVariable ["playerKDR", 0, true];
player setVariable ["longestPlayerKill", 0, true];
player setVariable ["longestAIKill", 0, true];

player setVariable ["forcePoints", 0, true];
player setVariable ["capturedPoints", 0, true];
player setVariable ["forcePointsGained", 0, true];
player setVariable ["forcePointsContributed", 0, true];
player setVariable ["forcePointsSpent", 0, true];

player setVariable ["killStreak", 0, true];
player setVariable ["largestKillStreak", 0, true];
player setVariable ["ammoCrateUsed", false, true];
player setVariable ["uavUsed", false, true];
player setVariable ["mortarStrikeUsed", false, true];
player setVariable ["ammoCratesUsed", 0, true];
player setVariable ["uavsUsed", 0, true];
player setVariable ["mortarsUsed", 0, true];

proceedWithInit = false;
[] call playerFunctions_fnc_loadPlayerData;

_setRating = 999999;
_getRating = rating player;
_addVal = _setRating - _getRating;
player addRating _addVal;

[] execVM "scripts\admin\loop.sqf";

[] call playerFunctions_fnc_assignGear;

["InitializePlayer", [player]] call BIS_fnc_dynamicGroups;

waitUntil {proceedWithInit};

[] execVM "scripts\serverFunctions\fn_statusBar.sqf";
[] execVM "scripts\serverFunctions\fn_rankBar.sqf";
[] execVM "scripts\serverFunctions\fn_newsfeed.sqf";
[] execVM "scripts\serverFunctions\handleNewsfeed.sqf";
[] execVM "scripts\serverFunctions\welcome.sqf";
[] execVM "scripts\serverFunctions\ambientSounds.sqf";

[] execVM "scripts\playerFunctions\killfeed\killfeed.sqf";		// KILLFEED
[] execVM "scripts\playerXPLoop.sqf";
[] execVM "scripts\playerKillStreakLoop.sqf";
[] execVM "scripts\player_markers.sqf";
[] execVM "scripts\radio.sqf";
[] execVM "scripts\nameTags.sqf";

[] execVM "briefing.sqf";

(findDisplay 46) displayAddEventHandler ["KeyDown", playerFunctions_fnc_keys];
[] call playerFunctions_fnc_healPlayer;
[] call playerFunctions_fnc_firefightMusic;

player enableFatigue false;
player enableStamina false;
player setCustomAimCoef 0;

waitUntil { !isNil { BIS_missionStarted } };
[] call playerFunctions_fnc_changeGear;
[] call playerFunctions_fnc_gearCheck;