private ["_AplayerUID""_Axp", "_Aprestige", "_AhumanKills", "_AfriendlyKills", "_AaiKills", "_AvehicleKills", "_AlongestPlayerKill", "_AlongestAIKill", "_AlargestKillStreak", "_AdeathsFromPlayers", "_AdeathsFromAI", "_AammoCratesUsed", "_AuavsUsed", "_AmortarsUsed", "_AcapturedPoints", "_AforcePointsGained", "_AforcePointsContributed", "_AforcePointsSpent", "_AaliveTime", "_BplayerUID""_Bxp", "_Bprestige", "_BhumanKills", "_BfriendlyKills", "_BaiKills", "_BvehicleKills", "_BlongestPlayerKill", "_BlongestAIKill", "_BlargestKillStreak", "_BdeathsFromPlayers", "_BdeathsFromAI", "_BammoCratesUsed", "_BuavsUsed", "_BmortarsUsed", "_BcapturedPoints", "_BforcePointsGained", "_BforcePointsContributed", "_BforcePointsSpent", "_BaliveTime"];

_AplayerUID = getPlayerUID player;
_Axp = player getVariable "playerXP";
_AhumanKills = player getVariable "totalPlayerKills";
_AfriendlyKills = player getVariable "totalFriendlyKills";
_AaiKills = player getVariable "totalAIKills";
_AvehicleKills = player getVariable "vehicleKills";
_AlongestPlayerKill = player getVariable "longestPlayerKill";
_AlongestAIKill = player getVariable "longestAIKill";
_AlargestKillStreak = player getVariable "largestKillStreak";
_AdeathsFromPlayers = player getVariable "deathsFromPlayers";
_AdeathsFromAI = player getVariable "deathsFromAI";
_AammoCratesUsed = player getVariable "ammoCratesUsed";
_AuavsUsed = player getVariable "uavsUsed";
_AmortarsUsed = player getVariable "mortarsUsed";
_AcapturedPoints = player getVariable "capturedPoints";
_AforcePointsGained = player getVariable "forcePointsGained";
_AforcePointsContributed = player getVariable "forcePointsContributed";
_AforcePointsSpent = player getVariable "forcePointsSpent";
_AaliveTime = player getVariable "aliveTime";

while {true} do {
	sleep 10;

	_BplayerUID = getPlayerUID player;
	_Bxp = player getVariable ["playerXP", 0];
	_BhumanKills = player getVariable ["totalPlayerKills", 0];
	_BfriendlyKills = player getVariable ["totalFriendlyKills", 0];
	_BaiKills = player getVariable ["totalAIKills", 0];
	_BvehicleKills = player getVariable ["vehicleKills", 0];
	_BlongestPlayerKill = player getVariable ["longestPlayerKill", 0];
	_BlongestAIKill = player getVariable ["longestAIKill", 0];
	_BlargestKillStreak = player getVariable ["largestKillStreak", 0];
	_BdeathsFromPlayers = player getVariable ["deathsFromPlayers", 0];
	_BdeathsFromAI = player getVariable ["deathsFromAI", 0];
	_BammoCratesUsed = player getVariable ["ammoCratesUsed", 0];
	_BuavsUsed = player getVariable ["uavsUsed", 0];
	_BmortarsUsed = player getVariable ["mortarsUsed", 0];
	_BcapturedPoints = player getVariable ["capturedPoints", 0];
	_BforcePointsGained = player getVariable ["forcePointsGained", 0];
	_BforcePointsContributed = player getVariable ["forcePointsContributed", 0];
	_BforcePointsSpent = player getVariable ["forcePointsSpent", 0];
	_BaliveTime = player getVariable ["aliveTime", 0];

	// Check if new values are less than old values. If so, revert value. XP can be 150 lower, no more
	if (_Bxp 						< (_Axp - 150)				) then { 	_Bxp 						= 	_Axp; 						player setVariable ["playerXP", 				_Axp, 						true]; };
	if (_BhumanKills 				< _AhumanKills				) then { 	_BhumanKills 				= 	_AhumanKills; 				player setVariable ["totalPlayerKills", 		_AhumanKills, 				true]; };
	if (_BfriendlyKills 			< _AfriendlyKills			) then { 	_BfriendlyKills 			= 	_AfriendlyKills; 			player setVariable ["totalFriendlyKills", 		_AfriendlyKills, 			true]; };
	if (_BaiKills 					< _AaiKills					) then { 	_BaiKills 					= 	_AaiKills; 					player setVariable ["totalAIKills", 			_AaiKills, 					true]; };
	if (_BvehicleKills 				< _AvehicleKills			) then { 	_BvehicleKills 				= 	_AvehicleKills; 			player setVariable ["vehicleKills", 			_AvehicleKills, 			true]; };
	if (_BlongestPlayerKill 		< _AlongestPlayerKill		) then { 	_BlongestPlayerKill 		= 	_AlongestPlayerKill; 		player setVariable ["longestPlayerKill", 		_AlongestPlayerKill, 		true]; };
	if (_BlongestAIKill 			< _AlongestAIKill			) then { 	_BlongestAIKill 			= 	_AlongestAIKill; 			player setVariable ["longestAIKill",			_AlongestAIKill, 			true]; };
	if (_BlargestKillStreak 		< _AlargestKillStreak		) then { 	_BlargestKillStreak 		= 	_AlargestKillStreak; 		player setVariable ["largestKillStreak", 		_AlargestKillStreak, 		true]; };
	if (_BdeathsFromPlayers 		< _AdeathsFromPlayers		) then { 	_BdeathsFromPlayers 		= 	_AdeathsFromPlayers; 		player setVariable ["deathsFromPlayers", 		_AdeathsFromPlayers, 		true]; };
	if (_BdeathsFromAI 				< _AdeathsFromAI			) then { 	_BdeathsFromAI 				= 	_AdeathsFromAI;			 	player setVariable ["deathsFromAI", 			_AdeathsFromAI, 			true]; };
	if (_BammoCratesUsed 			< _AammoCratesUsed			) then { 	_BammoCratesUsed			= 	_AammoCratesUsed; 			player setVariable ["ammoCratesUsed", 			_AammoCratesUsed, 			true]; };
	if (_BuavsUsed 					< _AuavsUsed				) then { 	_BuavsUsed 					= 	_AuavsUsed; 				player setVariable ["uavsUsed", 				_AuavsUsed, 				true]; };
	if (_BmortarsUsed 				< _AmortarsUsed				) then { 	_BmortarsUsed 				= 	_AmortarsUsed; 				player setVariable ["mortarsUsed", 				_AmortarsUsed, 				true]; };
	if (_BcapturedPoints 			< _AcapturedPoints			) then { 	_BcapturedPoints 			= 	_AcapturedPoints;			player setVariable ["capturedPoints", 			_AcapturedPoints, 			true]; };
	if (_BforcePointsGained 		< _AforcePointsGained		) then { 	_BforcePointsGained 		= 	_AforcePointsGained; 		player setVariable ["forcePointsGained", 		_AforcePointsGained, 		true]; };
	if (_BforcePointsContributed	< _AforcePointsContributed	) then { 	_BforcePointsContributed	=	_AforcePointsContributed;	player setVariable ["forcePointsContributed",	_AforcePointsContributed,	true]; };
	if (_BforcePointsSpent 			< _AforcePointsSpent		) then { 	_BforcePointsSpent 			= 	_AforcePointsSpent; 		player setVariable ["forcePointsSpent", 		_AforcePointsSpent, 		true]; };
	if (_BaliveTime 				< _AaliveTime				) then {	_BaliveTime 				= 	_AaliveTime; 				player setVariable ["aliveTime", 				_AaliveTime, 				true]; };

	[[_BplayerUID, _Bxp, _BhumanKills, _BfriendlyKills, _BaiKills, _BvehicleKills, _BlongestPlayerKill, _BlongestAIKill, _BlargestKillStreak, _BdeathsFromPlayers, _BdeathsFromAI, _BammoCratesUsed, _BuavsUsed, _BmortarsUsed, _BcapturedPoints, _BforcePointsGained, _BforcePointsContributed, _BforcePointsSpent, _BaliveTime], "Storm_fnc_setPlayerData", false, false, true] call BIS_fnc_MP;

	[] execVM "config\rankConfig.sqf";

	_AplayerUID = _BplayerUID;
	_Axp = _Bxp;
	_AhumanKills = _BhumanKills;
	_AfriendlyKills = _BfriendlyKills;
	_AaiKills = _BaiKills;
	_AvehicleKills = _BvehicleKills;
	_AlongestPlayerKill = _BlongestPlayerKill;
	_AlongestAIKill = _BlongestAIKill;
	_AlargestKillStreak = _BlargestKillStreak;
	_AdeathsFromPlayers = _BdeathsFromPlayers;
	_AdeathsFromAI = _BdeathsFromAI;
	_AammoCratesUsed = _BammoCratesUsed;
	_AuavsUsed = _BuavsUsed;
	_AmortarsUsed = _BmortarsUsed;
	_AcapturedPoints = _BcapturedPoints;
	_AforcePointsGained = _BforcePointsGained;
	_AforcePointsContributed = _BforcePointsContributed;
	_AforcePointsSpent = _BforcePointsSpent;
	_AaliveTime = _BaliveTime;
};
