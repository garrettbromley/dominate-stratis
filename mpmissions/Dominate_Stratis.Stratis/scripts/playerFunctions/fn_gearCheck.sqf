gearCheck_fnc_check = {
	private ["_whitelistedWeapons","_whitelistedItems","_whitelistedPacks","_allowedWeapons","_allowedPacks","_return","_allowedItems", "_currentItems", "_toSleep"];
	
	_whitelistedWeapons = ["arifle_Mk20_F","arifle_Mk20C_F","arifle_Mk20_GL_F","LMG_Mk200_F"];
	_whitelistedItems = [];
	_whitelistedPacks = [];
	
	// Get the list of allowed items
	_allowedWeapons = Arsweapons + _whitelistedWeapons;
	_allowedItems = Arsitems + _whitelistedItems;
	_allowedPacks = Arsbackpacks + _whitelistedPacks;
	
	_return = false;
	
	// Check Uniform
	if !(uniform player in _allowedItems || uniform player == "") then {
		removeUniform player;
		_return = true;
	};
	
	// Check Vest
	if !(vest player in _allowedItems || vest player == "") then {
		removeVest player;
		_return = true;
	};
	
	// Check Backpack
	if !(backpack player in _allowedPacks || backpack player == "") then {
		removeBackpack player;
		_return = true;
	};
	
	// Check Headgear
	if !(headgear player in _allowedItems || headgear player == "") then {
		removeHeadgear player;
		_return = true;
	};
	
	// Check Facewear
	if !(goggles player in _allowedItems || goggles player == "") then {
		removeGoggles player;
		_return = true;
	};
	
	// Check NVG's
	if !(hmd player in _allowedItems || hmd player == "") then {
		player unassignItem (hmd player);
		player removeItem (hmd player);
		_return = true;
	};
	
	// Check Binoculars
	if !(binocular player in _allowedItems || binocular player == "") then {
		if (currentWeapon player == binocular player) then {
			player removeWeapon (binocular player);
		} else {
			player unassignItem (binocular player);
			player removeItem (binocular player);
		};
		
		_return = true;
	};
	
	// Check Weapon
	if !(primaryWeapon player in _allowedWeapons || primaryWeapon player == "") then {
		player removeWeapon (primaryWeapon player);		
		_return = true;
	} else {
		// Check the accessories
		{
			if !(_x in _allowedItems || _x == "") then {
				player removePrimaryWeaponItem _x;
			};
		} forEach primaryWeaponItems player;
	};
	
	// Check Launcher
	if !(secondaryWeapon player in _allowedWeapons || secondaryWeapon player == "") then {
		player removeWeapon (secondaryWeapon player);
		_return = true;
	};
	
	// Check Pistol
	if !(handgunWeapon player in _allowedWeapons || handgunWeapon player == "") then {
		player removeWeapon (handgunWeapon player);
		_return = true;
	} else {
		// Check the accessories
		{
			if !(_x in _allowedItems || _x == "") then {
				player removeHandgunItem _x;
			};
		} forEach handgunItems player;
	};
	
	_return
};

[] spawn {
	while {true} do {
		private ["_gearCheckStatus"];
		
		// Run the gear check
		_gearCheckStatus = [] call gearCheck_fnc_check;
		
		// If they fail the gear check, tell them
		if (_gearCheckStatus) then {
			hint "You failed your gear inspection! You had items that are above your pay grade soldier!";
		};
		
		_toSleep = 10;
		// Check every 1 second when in the base building
		switch (playerSide) do {
			case west: {
				if (player distance2D (getMarkerPos "blu_spawn_zone") <= 150) then { _toSleep = 1; };
			};
			case east: {
				if (player distance2D (getMarkerPos "opf_spawn_zone") <= 150) then { _toSleep = 1; };
			};
		};
		
		sleep _toSleep;
	}
};