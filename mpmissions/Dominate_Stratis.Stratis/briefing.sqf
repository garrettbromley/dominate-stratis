if (!hasInterface) exitWith {};

waitUntil {!isNull player};

player createDiarySubject ["info", "Info and Help"];
player createDiarySubject ["ranks", "Ranking System"];
player createDiarySubject ["changelog", "Changelog"];
player createDiarySubject ["next", "Whats Next"];
player createDiarySubject ["credits", "Credits"];

player createDiaryRecord ["ranks",
[
"1 - Private 1",
"
<br/>
<br/>XP Required: 0
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"2 - Private 2",
"
<br/>
<br/>XP Required: 500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"3 - Private 3",
"
<br/>
<br/>XP Required: 1,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"4 - Private First Class 1",
"
<br/>
<br/>XP Required: 3,600
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"5 - Private First Class 2",
"
<br/>
<br/>XP Required: 6,200
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"6 - Private First Class 3",
"
<br/>
<br/>XP Required: 9,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"7 - Specialist 1",
"
<br/>
<br/>XP Required: 13,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"8 - Specialist 2",
"
<br/>
<br/>XP Required: 18,200
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"9 - Specialist 3",
"
<br/>
<br/>XP Required: 23,600
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"10 - Corporal 1",
"
<br/>
<br/>XP Required: 29,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"11 - Corporal 2",
"
<br/>
<br/>XP Required: 36,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"12 - Corporal 3",
"
<br/>
<br/>XP Required: 44,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"13 - Sergeant 1",
"
<br/>
<br/>XP Required: 53,100
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"14 - Sergeant 2",
"
<br/>
<br/>XP Required: 62,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"15 - Sergeant 3",
"
<br/>
<br/>XP Required: 73,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"16 - Staff Sergeant 1",
"
<br/>
<br/>XP Required: 85,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"17 - Staff Sergeant 2",
"
<br/>
<br/>XP Required: 96,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"18 - Staff Sergeant 3",
"
<br/>
<br/>XP Required: 121,100
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"19 - Sergeant First Class 1",
"
<br/>
<br/>XP Required: 126,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"20 - Sergeant First Class 2",
"
<br/>
<br/>XP Required: 142,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"21 - Sergeant First Class 3",
"
<br/>
<br/>XP Required: 159,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"22 - Master Sergeant 1",
"
<br/>
<br/>XP Required: 177,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"23 - Master Sergeant 2",
"
<br/>
<br/>XP Required: 196,100
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"24 - Master Sergeant 3",
"
<br/>
<br/>XP Required: 215,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"25 - First Sergeant 1",
"
<br/>
<br/>XP Required: 236,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"26 - First Sergeant 2",
"
<br/>
<br/>XP Required: 258,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"27 - First Sergeant 3",
"
<br/>
<br/>XP Required: 281,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"28 - Sergeant Major 1",
"
<br/>
<br/>XP Required: 305,100
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"29 - Sergeant Major 2",
"
<br/>
<br/>XP Required: 329,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"30 - Sergeant Major 3",
"
<br/>
<br/>XP Required: 355,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"31 - Command Sergeant Major 1",
"
<br/>
<br/>XP Required: 382,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"32 - Command Sergeant Major 2",
"
<br/>
<br/>XP Required: 410,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"33 - Command Sergeant Major 3",
"
<br/>
<br/>XP Required: 440,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"34 - Second Lieutenant 1",
"
<br/>
<br/>XP Required: 470,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"35 - Second Lieutenant 2",
"
<br/>
<br/>XP Required: 502,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"36 - Second Lieutenant 3",
"
<br/>
<br/>XP Required: 535,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"37 - First Lieutenant 1",
"
<br/>
<br/>XP Required: 569,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"38 - First Lieutenant 2",
"
<br/>
<br/>XP Required: 605,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"39 - First Lieutenant 3",
"
<br/>
<br/>XP Required: 641,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"40 - Captain 1",
"
<br/>
<br/>XP Required: 679,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"41 - Captain 2",
"
<br/>
<br/>XP Required: 718,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"42 - Captain 3",
"
<br/>
<br/>XP Required: 758,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"43 - Major 1",
"
<br/>
<br/>XP Required: 800,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"44 - Major 2",
"
<br/>
<br/>XP Required: 842,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"45 - Major 3",
"
<br/>
<br/>XP Required: 886,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"46 - Lieutenant Colonel 1",
"
<br/>
<br/>XP Required: 931,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"47 - Lieutenant Colonel 2",
"
<br/>
<br/>XP Required: 977,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"48 - Lieutenant Colonel 3",
"
<br/>
<br/>XP Required: 1,025,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"49 - Lieutenant Colonel 4",
"
<br/>
<br/>XP Required: 1,073,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"50 - Colonel 1",
"
<br/>
<br/>XP Required: 1,123,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"51 - Colonel 2",
"
<br/>
<br/>XP Required: 1,175,000
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"52 - Colonel 3",
"
<br/>
<br/>XP Required: 1,227,800
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"53 - Colonel 4",
"
<br/>
<br/>XP Required: 1,282,100
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"54 - Brigadier General 1",
"
<br/>
<br/>XP Required: 1,337,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"55 - Brigadier General 2",
"
<br/>
<br/>XP Required: 1,395,200
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"56 - Brigadier General 3",
"
<br/>
<br/>XP Required: 1,454,000
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"57 - Brigadier General 4",
"
<br/>
<br/>XP Required: 1,514,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"58 - Major General 1",
"
<br/>
<br/>XP Required: 1,576,100
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"59 - Major General 2",
"
<br/>
<br/>XP Required: 1,639,400
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"60 - Major General 3",
"
<br/>
<br/>XP Required: 1,704,200
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"61 - Major General 4",
"
<br/>
<br/>XP Required: 1,770,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"62 - Lieutenant General 1",
"
<br/>
<br/>XP Required: 1,838,300
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"63 - Lieutenant General 2",
"
<br/>
<br/>XP Required: 1,907,600
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"64 - Lieutenant General 3",
"
<br/>
<br/>XP Required: 1,978,400
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"65 - Lieutenant General 4",
"
<br/>
<br/>XP Required: 2,050,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"66 - General 1",
"
<br/>
<br/>XP Required: 2,124,500
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"67 - General 2",
"
<br/>
<br/>XP Required: 2,199,800
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"68 - General 3",
"
<br/>
<br/>XP Required: 2,276,600
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"69 - General 4",
"
<br/>
<br/>XP Required: 2,354,900
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

player createDiaryRecord ["ranks",
[
"70 - Commander",
"
<br/>
<br/>XP Required: 2,434,700
<br/>
<br/>Unlocked at this Level:
<br/>
<br/>
"
]];

/*player createDiaryRecord ["changelog",
[
"v1.0",
"
<br/>
<br/>[Added] Safe Zones
<br/>[Added] Player Boundaries
<br/>[Added] Ambient Sound
<br/>[Added] Intro Video and UAV Shot
<br/>[Added] Spawn exits blocked till 1 minute into match
<br/>[Added] Ranking System
<br/>[Added] Database Saves
<br/>[Added] Virtual Arsenal Loadouts
<br/>[Added] Save Gear upon leaving spawn
<br/>[Added] Tickets on HUD
<br/>[Added] Add XP for each Kill
<br/>[Added] Friendly Fire Detection
<br/>[Added] Kick to lobby after 3 TKs
<br/>[Added] View Distance Settings
<br/>[Added] Admin Tools
<br/>[Added] Player Markers
<br/>[Added] FPS Counter
<br/>[Added] Player Counter
<br/>[Added] Grid Reference Bar
<br/>[Added] Rank Info HUD
<br/>[Added] KDR Info in HUD
<br/>[Added] Rank Icons Optimized
<br/>[Added] Suicide Detection
<br/>
"
]];*/

/*player createDiaryRecord ["changelog",
[
"v1.1",
"
<br/>[Changed] Code Optimization
"
]];*/

/*player createDiaryRecord ["next",
[
"Coming Soon",
"
<br/>
<br/>	* Code Optimization
<br/>	* Scripts moved Server Side
<br/>	* Kill Streak Rewards
<br/>	* Show Kill Streak In HUD
<br/>	* Extra XP On Killstreaks
<br/>	* Show Shots Fired/Hit (Accuracy)
<br/>	* Prestige System
<br/>	* Assists on Kills
<br/>	* More XP for Pistol Kills
<br/>
"
]];*/

player createDiaryRecord ["credits",
[
"Credits",
"
<br/><font size='16' color='#BBBBBB'>Developed by Garrett Bromley</font>
<br/>	* Otherwise known as Black Saber XI
<br/>
<br/><font size='16' color='#BBBBBB'>Scripts Used:</font>
<br/>	* extDB2 by Torndeco
<br/>	* TAW View Distance Script by Tonic
<br/>
"
]];


player createDiaryRecord ["info",
[
"Hints and Tips",
"
<br/>* Work together as a team. One man with a gun can be dangerous, 10 men with guns working as a team is down right insanity.
"
]];

player createDiaryRecord ["info",
["Custom Key Bindings",
"<br/>Put earplugs in: F1 or Custom User Action 1
<br/>Increase Volume with Earplugs: F2 or Custom User Action 2
<br/>Decrease Volume with Earplugs: F3 or Custom User Action 3
<br/>Take earplugs out: F4 or Custom User Action 4
"
]];

player createDiaryRecord ["info",
[
"About The Storm Series",
"
<br/>Storm  is a team versus team match to the death. The objective of this mission is to rally your team up and use squad based tactics to kill the enemy team repeatedly until they run out of tickets.
<br/>
<br/>FAQ:
<br/>
<br/>Q. To be completed
<br/>
"
]];
