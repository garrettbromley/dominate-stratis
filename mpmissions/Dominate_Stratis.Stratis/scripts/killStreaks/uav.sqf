private ["_vehicle", "_veh", "_sp", "_uavsUsed"];

_uavsUsed = player getVariable "uavsUsed";
_uavsUsed = _uavsUsed + 1;
player setVariable ["uavsUsed", _uavsUsed, true];

player removeAction uavReward;
player setVariable ["uavUsed", true, true];
uavReward = nil;

player sidechat "Setting up your Quad Rotor UAV...";
player playMove "AinvPknlMstpSnonWnonDnon_medic_1";
sleep 3;

_sp = [(getpos player select 0) + 0, (getpos player select 1) + 0, getpos player select 2];

switch (side player) do {
	case west: {_veh = "B_UAV_01_F"; player linkItem "B_UavTerminal";};
	case east: {_veh = "O_UAV_01_F"; player linkItem "O_UavTerminal";};
};

_vehicle = _veh createVehicle _sp;
createVehicleCrew _vehicle;

player sideChat "UAV Set up. Double-tap Tab to show live feed...";

waitUntil {(!alive _vehicle)};

sleep 10;

deleteVehicle _vehicle;
