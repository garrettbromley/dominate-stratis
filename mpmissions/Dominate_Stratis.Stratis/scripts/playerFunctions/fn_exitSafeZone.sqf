private ["_playerUID", "_playerGear", "_playerSide", "_playerUnit"];

[] call playerFunctions_fnc_gatherGear;

_playerUID = (getPlayerUID player);
_playerGear = player getVariable "playerGear";

if (side player == WEST) then {
	_playerSide = "WEST";
};

if (side player == EAST) then {
	_playerSide = "EAST";
};

[[_playerUID, _playerGear, _playerSide], "Storm_fnc_setPlayerGear", false, false, true] call BIS_fnc_MP;

titleFadeOut 1;

player removeEventHandler ["Fired", 0];
player allowDamage true;
