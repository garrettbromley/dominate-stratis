if (isServer) exitWith {};

private ["_private1", "_private2", "_private3", "_privateFirstClass1", "_privateFirstClass2", "_privateFirstClass3", "_specialist1", "_specialist2", "_specialist3", "_corporal1", "_corporal2", "_corporal3", "_sergeant1", "_sergeant2", "_sergeant3", "_staffSergeant1", "_staffSergeant2", "_staffSergeant3", "_sergeantFirstClass1", "_sergeantFirstClass2", "_sergeantFirstClass3", "_masterSergeant1", "_masterSergeant2", "_masterSergeant3", "_firstSergeant1", "_firstSergeant2", "_firstSergeant3", "_sergeantMajor1", "_sergeantMajor2", "_sergeantMajor3", "_commandSergeantMajor1", "_commandSergeantMajor2", "_commandSergeantMajor3", "_secondLieutenant1", "_secondLieutenant2", "_secondLieutenant3", "_firstLieutenant1", "_firstLieutenant2", "_firstLieutenant3", "_captain1", "_captain2", "_captain3", "_major1", "_major2", "_major3", "_lieutenantColonel1", "_lieutenantColonel2", "_lieutenantColonel3", "_lieutenantColonel4", "_colonel1", "_colonel2", "_colonel3", "_colonel4", "_brigadierGeneral1", "_brigadierGeneral2", "_brigadierGeneral3", "_brigadierGeneral4", "_majorGeneral1", "_majorGeneral2", "_majorGeneral3", "_majorGeneral4", "_lieutenantGeneral1", "_lieutenantGeneral2", "_lieutenantGeneral3", "_lieutenantGeneral4", "_general1", "_general2", "_general3", "_general4", "_commander"];

_private1 = private1;
_private2 = private2;
_private3 = private3;
_privateFirstClass1 = privateFirstClass1;
_privateFirstClass2 = privateFirstClass2;
_privateFirstClass3 = privateFirstClass3;
_specialist1 = specialist1;
_specialist2 = specialist2;
_specialist3 = specialist3;
_corporal1 = corporal1;
_corporal2 = corporal2;
_corporal3 = corporal3;
_sergeant1 = sergeant1;
_sergeant2 = sergeant2;
_sergeant3 = sergeant3;
_staffSergeant1 = staffSergeant1;
_staffSergeant2 = staffSergeant2;
_staffSergeant3 = staffSergeant3;
_sergeantFirstClass1 = sergeantFirstClass1;
_sergeantFirstClass2 = sergeantFirstClass2;
_sergeantFirstClass3 = sergeantFirstClass3;
_masterSergeant1 = masterSergeant1;
_masterSergeant2 = masterSergeant2;
_masterSergeant3 = masterSergeant3;
_firstSergeant1 = firstSergeant1;
_firstSergeant2 = firstSergeant2;
_firstSergeant3 = firstSergeant3;
_sergeantMajor1 = sergeantMajor1;
_sergeantMajor2 = sergeantMajor2;
_sergeantMajor3 = sergeantMajor3;
_commandSergeantMajor1 = commandSergeantMajor1;
_commandSergeantMajor2 = commandSergeantMajor2;
_commandSergeantMajor3 = commandSergeantMajor3;
_secondLieutenant1 = secondLieutenant1;
_secondLieutenant2 = secondLieutenant2;
_secondLieutenant3 = secondLieutenant3;
_firstLieutenant1 = firstLieutenant1;
_firstLieutenant2 = firstLieutenant2;
_firstLieutenant3 = firstLieutenant3;
_captain1 = captain1;
_captain2 = captain2;
_captain3 = captain3;
_major1 = major1;
_major2 = major2;
_major3 = major3;
_lieutenantColonel1 = lieutenantColonel1;
_lieutenantColonel2 = lieutenantColonel2;
_lieutenantColonel3 = lieutenantColonel3;
_lieutenantColonel4 = lieutenantColonel4;
_colonel1 = colonel1;
_colonel2 = colonel2;
_colonel3 = colonel3;
_colonel4 = colonel4;
_brigadierGeneral1 = brigadierGeneral1;
_brigadierGeneral2 = brigadierGeneral2;
_brigadierGeneral3 = brigadierGeneral3;
_brigadierGeneral4 = brigadierGeneral4;
_majorGeneral1 = majorGeneral1;
_majorGeneral2 = majorGeneral2;
_majorGeneral3 = majorGeneral3;
_majorGeneral4 = majorGeneral4;
_lieutenantGeneral1 = lieutenantGeneral1;
_lieutenantGeneral2 = lieutenantGeneral2;
_lieutenantGeneral3 = lieutenantGeneral3;
_lieutenantGeneral4 = lieutenantGeneral4;
_general1 = general1;
_general2 = general2;
_general3 = general3;
_general4 = general4;
_commander = commander1;


playerXP = player getVariable "playerXP";

if ((playerXP >= _private1) && (playerXP < _private2)) then {
	player setVariable ["playerRankNo", 1, true];
	player setVariable ["playerRankName", "Private 1", true];
	player setVariable ["playerRankIcon", "media\ranks\private.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _private2) && (playerXP < _private3)) then {
	player setVariable ["playerRankNo", 2, true];
	player setVariable ["playerRankName", "Private 2", true];
	player setVariable ["playerRankIcon", "media\ranks\private.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _private3) && (playerXP < _privateFirstClass1)) then {
	player setVariable ["playerRankNo", 3, true];
	player setVariable ["playerRankName", "Private 3", true];
	player setVariable ["playerRankIcon", "media\ranks\private.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _privateFirstClass1) && (playerXP < _privateFirstClass2)) then {
	player setVariable ["playerRankNo", 4, true];
	player setVariable ["playerRankName", "Private First Class 1", true];
	player setVariable ["playerRankIcon", "media\ranks\privateFirstClass.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _privateFirstClass2) && (playerXP < _privateFirstClass3)) then {
	player setVariable ["playerRankNo", 5, true];
	player setVariable ["playerRankName", "Private First Class 2", true];
	player setVariable ["playerRankIcon", "media\ranks\privateFirstClass.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _privateFirstClass3) && (playerXP < _specialist1)) then {
	player setVariable ["playerRankNo", 6, true];
	player setVariable ["playerRankName", "Private First Class 3", true];
	player setVariable ["playerRankIcon", "media\ranks\privateFirstClass.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _specialist1) && (playerXP < _specialist2)) then {
	player setVariable ["playerRankNo", 7, true];
	player setVariable ["playerRankName", "Specialist 1", true];
	player setVariable ["playerRankIcon", "media\ranks\specialist.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _specialist2) && (playerXP < _specialist3)) then {
	player setVariable ["playerRankNo", 8, true];
	player setVariable ["playerRankName", "Specialist 2", true];
	player setVariable ["playerRankIcon", "media\ranks\specialist.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _specialist3) && (playerXP < _corporal1)) then {
	player setVariable ["playerRankNo", 9, true];
	player setVariable ["playerRankName", "Specialist 3", true];
	player setVariable ["playerRankIcon", "media\ranks\specialist.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _corporal1) && (playerXP < _corporal2)) then {
	player setVariable ["playerRankNo", 10, true];
	player setVariable ["playerRankName", "Corporal 1", true];
	player setVariable ["playerRankIcon", "media\ranks\corporal.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _corporal2) && (playerXP < _corporal3)) then {
	player setVariable ["playerRankNo", 11, true];
	player setVariable ["playerRankName", "Corporal 2", true];
	player setVariable ["playerRankIcon", "media\ranks\corporal.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _corporal3) && (playerXP < _sergeant1)) then {
	player setVariable ["playerRankNo", 12, true];
	player setVariable ["playerRankName", "Corporal 3", true];
	player setVariable ["playerRankIcon", "media\ranks\corporal.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeant1) && (playerXP < _sergeant2)) then {
	player setVariable ["playerRankNo", 13, true];
	player setVariable ["playerRankName", "Sergeant 1", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeant2) && (playerXP < _sergeant3)) then {
	player setVariable ["playerRankNo", 14, true];
	player setVariable ["playerRankName", "Sergeant 2", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeant3) && (playerXP < _staffSergeant1)) then {
	player setVariable ["playerRankNo", 15, true];
	player setVariable ["playerRankName", "Sergeant 3", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _staffSergeant1) && (playerXP < _staffSergeant2)) then {
	player setVariable ["playerRankNo", 16, true];
	player setVariable ["playerRankName", "Staff Sergeant 1", true];
	player setVariable ["playerRankIcon", "media\ranks\staffSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _staffSergeant2) && (playerXP < _staffSergeant3)) then {
	player setVariable ["playerRankNo", 17, true];
	player setVariable ["playerRankName", "Staff Sergeant 2", true];
	player setVariable ["playerRankIcon", "media\ranks\staffSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _staffSergeant3) && (playerXP < _sergeantFirstClass1)) then {
	player setVariable ["playerRankNo", 18, true];
	player setVariable ["playerRankName", "Staff Sergeant 3", true];
	player setVariable ["playerRankIcon", "media\ranks\staffSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeantFirstClass1) && (playerXP < _sergeantFirstClass2)) then {
	player setVariable ["playerRankNo", 19, true];
	player setVariable ["playerRankName", "Sergeant First Class 1", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeantFirstClass.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeantFirstClass2) && (playerXP < _sergeantFirstClass3)) then {
	player setVariable ["playerRankNo", 20, true];
	player setVariable ["playerRankName", "Sergeant First Class 2", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeantFirstClass.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeantFirstClass3) && (playerXP < _masterSergeant1)) then {
	player setVariable ["playerRankNo", 21, true];
	player setVariable ["playerRankName", "Sergeant First Glass 3", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeantFirstClass.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _masterSergeant1) && (playerXP < _masterSergeant2)) then {
	player setVariable ["playerRankNo", 22, true];
	player setVariable ["playerRankName", "Master Sergeant 1", true];
	player setVariable ["playerRankIcon", "media\ranks\masterSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _masterSergeant2) && (playerXP < _masterSergeant3)) then {
	player setVariable ["playerRankNo", 23, true];
	player setVariable ["playerRankName", "Master Sergeant2", true];
	player setVariable ["playerRankIcon", "media\ranks\masterSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _masterSergeant3) && (playerXP < _firstSergeant1)) then {
	player setVariable ["playerRankNo", 24, true];
	player setVariable ["playerRankName", "Master Sergeant 3", true];
	player setVariable ["playerRankIcon", "media\ranks\masterSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _firstSergeant1) && (playerXP < _firstSergeant2)) then {
	player setVariable ["playerRankNo", 25, true];
	player setVariable ["playerRankName", "First Sergeant 1", true];
	player setVariable ["playerRankIcon", "media\ranks\firstSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _firstSergeant2) && (playerXP < _firstSergeant3)) then {
	player setVariable ["playerRankNo", 26, true];
	player setVariable ["playerRankName", "First Sergeant 2", true];
	player setVariable ["playerRankIcon", "media\ranks\firstSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _firstSergeant3) && (playerXP < _sergeantMajor1)) then {
	player setVariable ["playerRankNo", 27, true];
	player setVariable ["playerRankName", "First Sergeant 3", true];
	player setVariable ["playerRankIcon", "media\ranks\firstSergeant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeantMajor1) && (playerXP < _sergeantMajor2)) then {
	player setVariable ["playerRankNo", 28, true];
	player setVariable ["playerRankName", "Sergeant Major 1", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeantMajor.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeantMajor2) && (playerXP < _sergeantMajor3)) then {
	player setVariable ["playerRankNo", 29, true];
	player setVariable ["playerRankName", "Sergeant Major 2", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeantMajor.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _sergeantMajor3) && (playerXP < _commandSergeantMajor1)) then {
	player setVariable ["playerRankNo", 30, true];
	player setVariable ["playerRankName", "Sergeant Major 3", true];
	player setVariable ["playerRankIcon", "media\ranks\sergeantMajor.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _commandSergeantMajor1) && (playerXP < _commandSergeantMajor2)) then {
	player setVariable ["playerRankNo", 31, true];
	player setVariable ["playerRankName", "Command Sergeant Major 1", true];
	player setVariable ["playerRankIcon", "media\ranks\commandSergeantMajor.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _commandSergeantMajor2) && (playerXP < _commandSergeantMajor3)) then {
	player setVariable ["playerRankNo", 32, true];
	player setVariable ["playerRankName", "Command Sergeant Major 2", true];
	player setVariable ["playerRankIcon", "media\ranks\commandSergeantMajor.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _commandSergeantMajor3) && (playerXP < _secondLieutenant1)) then {
	player setVariable ["playerRankNo", 33, true];
	player setVariable ["playerRankName", "Command Sergeant Major 3", true];
	player setVariable ["playerRankIcon", "media\ranks\commandSergeantMajor.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _secondLieutenant1) && (playerXP < _secondLieutenant2)) then {
	player setVariable ["playerRankNo", 34, true];
	player setVariable ["playerRankName", "Second Lieutenant 1", true];
	player setVariable ["playerRankIcon", "media\ranks\secondLieutenant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _secondLieutenant2) && (playerXP < _secondLieutenant3)) then {
	player setVariable ["playerRankNo", 35, true];
	player setVariable ["playerRankName", "Second Lieutenant 2", true];
	player setVariable ["playerRankIcon", "media\ranks\secondLieutenant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _secondLieutenant3) && (playerXP < _firstLieutenant1)) then {
	player setVariable ["playerRankNo", 36, true];
	player setVariable ["playerRankName", "Second Lieutenant 3", true];
	player setVariable ["playerRankIcon", "media\ranks\secondLieutenant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _firstLieutenant1) && (playerXP < _firstLieutenant2)) then {
	player setVariable ["playerRankNo", 37, true];
	player setVariable ["playerRankName", "First Lieutenant 1", true];
	player setVariable ["playerRankIcon", "media\ranks\firstLieutenant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _firstLieutenant2) && (playerXP < _firstLieutenant3)) then {
	player setVariable ["playerRankNo", 38, true];
	player setVariable ["playerRankName", "First Lieutenant 2", true];
	player setVariable ["playerRankIcon", "media\ranks\firstLieutenant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _firstLieutenant3) && (playerXP < _captain1)) then {
	player setVariable ["playerRankNo", 39, true];
	player setVariable ["playerRankName", "First Lieutenant 3", true];
	player setVariable ["playerRankIcon", "media\ranks\firstLieutenant.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _captain1) && (playerXP < _captain2)) then {
	player setVariable ["playerRankNo", 40, true];
	player setVariable ["playerRankName", "Captain 1", true];
	player setVariable ["playerRankIcon", "media\ranks\captain.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _captain2) && (playerXP < _captain3)) then {
	player setVariable ["playerRankNo", 41, true];
	player setVariable ["playerRankName", "Captain 2", true];
	player setVariable ["playerRankIcon", "media\ranks\captain.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _captain3) && (playerXP < _major1)) then {
	player setVariable ["playerRankNo", 42, true];
	player setVariable ["playerRankName", "Captain 3", true];
	player setVariable ["playerRankIcon", "media\ranks\captain.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _major1) && (playerXP < _major2)) then {
	player setVariable ["playerRankNo", 43, true];
	player setVariable ["playerRankName", "Major 1", true];
	player setVariable ["playerRankIcon", "media\ranks\major.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _major2) && (playerXP < _major3)) then {
	player setVariable ["playerRankNo", 44, true];
	player setVariable ["playerRankName", "Major 2", true];
	player setVariable ["playerRankIcon", "media\ranks\major.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _major3) && (playerXP < _lieutenantColonel1)) then {
	player setVariable ["playerRankNo", 45, true];
	player setVariable ["playerRankName", "Major 3", true];
	player setVariable ["playerRankIcon", "media\ranks\major.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantColonel1) && (playerXP < _lieutenantColonel2)) then {
	player setVariable ["playerRankNo", 46, true];
	player setVariable ["playerRankName", "Lieutenant Colonel 1", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantColonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantColonel2) && (playerXP < _lieutenantColonel3)) then {
	player setVariable ["playerRankNo", 47, true];
	player setVariable ["playerRankName", "Lieutenant Colonel 2", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantColonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantColonel3) && (playerXP < _lieutenantColonel4)) then {
	player setVariable ["playerRankNo", 48, true];
	player setVariable ["playerRankName", "Lieutenant Colonel 3", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantColonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantColonel4) && (playerXP < _colonel1)) then {
	player setVariable ["playerRankNo", 49, true];
	player setVariable ["playerRankName", "Lieutenant Colonel 4", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantColonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _colonel1) && (playerXP < _colonel2)) then {
	player setVariable ["playerRankNo", 50, true];
	player setVariable ["playerRankName", "Colonel 1", true];
	player setVariable ["playerRankIcon", "media\ranks\colonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _colonel2) && (playerXP < _colonel3)) then {
	player setVariable ["playerRankNo", 51, true];
	player setVariable ["playerRankName", "Colonel 2", true];
	player setVariable ["playerRankIcon", "media\ranks\colonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _colonel3) && (playerXP < _colonel4)) then {
	player setVariable ["playerRankNo", 52, true];
	player setVariable ["playerRankName", "Colonel 3", true];
	player setVariable ["playerRankIcon", "media\ranks\colonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _colonel4) && (playerXP < _brigadierGeneral1)) then {
	player setVariable ["playerRankNo", 53, true];
	player setVariable ["playerRankName", "Colonel 4", true];
	player setVariable ["playerRankIcon", "media\ranks\colonel.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _brigadierGeneral1) && (playerXP < _brigadierGeneral2)) then {
	player setVariable ["playerRankNo", 54, true];
	player setVariable ["playerRankName", "Brigadier General 1", true];
	player setVariable ["playerRankIcon", "media\ranks\brigadierGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _brigadierGeneral2) && (playerXP < _brigadierGeneral3)) then {
	player setVariable ["playerRankNo", 55, true];
	player setVariable ["playerRankName", "Brigadier General 2", true];
	player setVariable ["playerRankIcon", "media\ranks\brigadierGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _brigadierGeneral3) && (playerXP < _brigadierGeneral4)) then {
	player setVariable ["playerRankNo", 56, true];
	player setVariable ["playerRankName", "Brigadier General 3", true];
	player setVariable ["playerRankIcon", "media\ranks\brigadierGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _brigadierGeneral4) && (playerXP < _majorGeneral1)) then {
	player setVariable ["playerRankNo", 57, true];
	player setVariable ["playerRankName", "Brigadier General 4", true];
	player setVariable ["playerRankIcon", "media\ranks\brigadierGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _majorGeneral1) && (playerXP < _majorGeneral2)) then {
	player setVariable ["playerRankNo", 58, true];
	player setVariable ["playerRankName", "Major General 1", true];
	player setVariable ["playerRankIcon", "media\ranks\majorGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _majorGeneral2) && (playerXP < _majorGeneral3)) then {
	player setVariable ["playerRankNo", 59, true];
	player setVariable ["playerRankName", "Major General 2", true];
	player setVariable ["playerRankIcon", "media\ranks\majorGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _majorGeneral3) && (playerXP < _majorGeneral4)) then {
	player setVariable ["playerRankNo", 60, true];
	player setVariable ["playerRankName", "Major General 3", true];
	player setVariable ["playerRankIcon", "media\ranks\majorGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _majorGeneral4) && (playerXP < _lieutenantGeneral1)) then {
	player setVariable ["playerRankNo", 61, true];
	player setVariable ["playerRankName", "Major General 4", true];
	player setVariable ["playerRankIcon", "media\ranks\majorGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantGeneral1) && (playerXP < _lieutenantGeneral2)) then {
	player setVariable ["playerRankNo", 62, true];
	player setVariable ["playerRankName", "Lieutenant General 1", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantGeneral2) && (playerXP < _lieutenantGeneral3)) then {
	player setVariable ["playerRankNo", 63, true];
	player setVariable ["playerRankName", "Lieutenant General 2", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantGeneral3) && (playerXP < _lieutenantGeneral4)) then {
	player setVariable ["playerRankNo", 64, true];
	player setVariable ["playerRankName", "Lieutenant General 3", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _lieutenantGeneral4) && (playerXP < _general1)) then {
	player setVariable ["playerRankNo", 65, true];
	player setVariable ["playerRankName", "Lieutenant General 4", true];
	player setVariable ["playerRankIcon", "media\ranks\lieutenantGeneral.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _general1) && (playerXP < _general2)) then {
	player setVariable ["playerRankNo", 66, true];
	player setVariable ["playerRankName", "General 1", true];
	player setVariable ["playerRankIcon", "media\ranks\general.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _general2) && (playerXP < _general3)) then {
	player setVariable ["playerRankNo", 67, true];
	player setVariable ["playerRankName", "General 2", true];
	player setVariable ["playerRankIcon", "media\ranks\general.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _general3) && (playerXP < _general4)) then {
	player setVariable ["playerRankNo", 68, true];
	player setVariable ["playerRankName", "General 3", true];
	player setVariable ["playerRankIcon", "media\ranks\general.jpg", true];
	if(true) exitWith{};
};

if ((playerXP >= _general4) && (playerXP < _commander)) then {
	player setVariable ["playerRankNo", 69, true];
	player setVariable ["playerRankName", "General 4", true];
	player setVariable ["playerRankIcon", "media\ranks\general.jpg", true];
	if(true) exitWith{};
};

if (playerXP >= _commander) then {
	player setVariable ["playerRankNo", 70, true];
	player setVariable ["playerRankName", "Commander", true];
	player setVariable ["playerRankIcon", "media\ranks\commander.jpg", true];
	if(true) exitWith{};
};
