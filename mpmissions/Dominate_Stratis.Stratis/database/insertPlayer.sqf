if (!isServer) exitWith {};

private ["_name", "_UID"];

_name = _this select 0;
_UID = _this select 1;

[format ["insertPlayer:%1:%2", _name, _UID]] call extDB_fnc_async;
