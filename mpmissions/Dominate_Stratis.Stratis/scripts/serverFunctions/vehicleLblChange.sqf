#define getControl(disp,ctrl) ((findDisplay ##disp) displayCtrl ##ctrl)

fnc_numberText = {
	private ["_number", "_mod", "_digits", "_digitsCount", "_modBase", "_numberText"];

	_number = [_this,0,0,[0]] call bis_fnc_param;
	_mod = [_this,1,3,[0]] call bis_fnc_param;
	_digits = _number call bis_fnc_numberDigits;

	_digitsCount = count _digits - 1;
	_modBase = _digitsCount % _mod;
	_numberText = "";

	{
		_numberText = _numberText + str _x;
		if ((_foreachindex - _modBase) % (_mod) == 0 && _foreachindex != _digitsCount) then {
			_numberText = _numberText + ", ";
		};
	} foreach _digits;

	_numberText
};

disableSerialization;

private["_control", "_index", "_className", "_basePrice", "_vehicleInfo", "_ctrl"];

_control = (_this select 0) select 0;
_index = (_this select 0) select 1;

_className = _control lbData _index;
_vIndex = _control lbValue _index;
_basePrice = (vehicleList select _vIndex) select 1;

_vehicleInfo = [_className] call fnc_fetchVehInfo;

ctrlShow [2330,true];

(getControl(2300,2303)) ctrlSetStructuredText parseText format["Force Points Required: <t color='#8cff9b'>%1</t><br/>Max Speed: %2 km/h<br/>Horse Power: %3<br/>Passenger Seats: %4<br/>Fuel Capacity: %5<br/>Armor Rating: %6",[_basePrice] call fnc_numberText,_vehicleInfo select 8,_vehicleInfo select 11,
_vehicleInfo select 10,_vehicleInfo select 12,_vehicleInfo select 9];

_ctrl = getControl(2300,2304);

_availablePoints = ((player getVariable "currentStore") getVariable "availablePoints") + (player getVariable "forcePoints");

(getControl(2300,2301)) ctrlSetText format["Available Force Points: %1", _availablePoints];

lbClear _ctrl;
lbSetCurSel[2304,0];

if ((lbSize _ctrl) -1 != -1) then {
	ctrlShow[2304,true];
} else {
	ctrlShow[2304,false];
};

true;
