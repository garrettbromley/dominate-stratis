params [
	"_Unit",
	"_Container"
];

if (getText (configfile >> "CfgVehicles" >> typeOf _Container >> "vehicleClass") == "Backpacks") then {
	if !(_Unit getVariable "Backpack_Allow_Access") then {
		if (local player && player != _Unit) then {
			hintSilent format ["%1's backpack is currently locked.", name _Unit];
			true;
		};
	};
};
