// Monitor Groups

aiAliveGroups = [];
aiAliveGroupsTransported = [];

while {true} do {
	private ["_arraySize", "_helperArray", "_unitCount"];

	_arraySize = count aiAliveGroups;
	
	if (_arraySize > 0) then {
		_helperArray = [];
		
		{
			_unitCount = {alive _x} count units _x;
			
			if (_unitCount > 0 && !(_x isEqualTo grpNull)) then { _helperArray pushBack _x };
		} forEach aiAliveGroups;
		
		aiAliveGroups = _helperArray;
	};
	
	sleep 10;
};