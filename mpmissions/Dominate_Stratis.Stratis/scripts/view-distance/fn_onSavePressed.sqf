#include "defines.h"

private ["_saveIndex","_saveName"];
_saveIndex = lbCurSel SAVES_LIST;
_saveName = ctrlText SLOT_NAME;

SVAR_PNS [format["tawvd_slot_%1",_saveIndex],
	[
		_saveName,
		tawvd_foot,
		tawvd_car,
		tawvd_air,
		tawvd_drone,
		tawvd_object,
		tawvd_syncObject
	]
];

saveProfileNamespace;
[] call TAWVD_fnc_openSaveManager;
