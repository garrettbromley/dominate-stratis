class playerFunctions
{
	class gearFunctions
	{
		file = "scripts\playerFunctions";
		class changeGear {};
		class gatherGear {};
		class assignGear {};
		class allowGear {};
		class disableGear {};
		class gearCheck {};
	};

	class config
	{
		file = "config";
		class gearConfig {};
	};

	class safeZoneFunctions
	{
		file = "scripts\playerFunctions";
		class safeZone {};
		class exitSafeZone {};
		class enterSpawn {};
		class enterPlayerArea {};
		class exitPlayerArea {};
	};

	class dataFunctions
	{
		file = "scripts\playerFunctions";
		class loadPlayerData {};
	};

	class otherFunctions
	{
		file = "scripts\playerFunctions";
		class keys {};
		class healPlayer {};
		class openPack {};
		class firefightMusic {};
		class playMusic {};
	};
};

class overrideVATemplates
{
	tag = "arsenalFunctions";
	class Inventory
	{
		file = "scripts\arsenalFunctions";
		class initOverride { postInit = 1; };
		class loadInventory_whiteList {};
		class overrideVAButtonDown {};
		class overrideVATemplateOK {};
	};
};

class killfeed {
	tag = "kill";
	class functions {
		file = "scripts\playerFunctions\killfeed\functions";
		class killfeedMain {};
		class killfeedCompile {};
		class killfeedType {};
		class killfeedRender {};
	};
};

class extDB
{
	class Database
	{
		file = "database\extDB";
		class async {};
		class strip {};
	};
};
