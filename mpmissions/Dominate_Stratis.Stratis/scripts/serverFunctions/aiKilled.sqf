private ["_killedAI", "_killer", "_killersXP", "_killersKills", "_killerNewsFeed", "_newKillersKills", "_xpForKill", "_newXP", "_killFeed", "_killerNewsFeed"];

_killedAI = _this select 0;
if (isNull (_this select 2)) then {
	_killer = _this select 1;
} else {
	_killer = _this select 2;
};

if !(isPlayer _killer) exitWith {};

_killersXP = _killer getVariable "playerXP";
_killersKills = _killer getVariable "playerKills";
_killersAIKills = _killer getVariable "totalAIKills";
_killersLongest = _killer getVariable "longestAIKill";
_killerNewsFeed = _killer getVariable "playerNews";

_newKillersKills = _killersKills + 1;
_newKillersAIKills = _killersAIKills + 1;
_xpForKill = 50;
_newXP = _killersXP + _xpForKill;
_killFeed = format ["[ %1+ ] You killed an AI", _xpForKill];
_killerNewsFeed pushBack [_killFeed, 10];

_killDistance = _killedAI distance _killer;
if (_killDistance > _killersLongest) then {
	_killer setVariable ["longestAIKill", _killDistance, true];
};

_killer setVariable ["playerXP", _newXP, true];
_killer setVariable ["playerKills", _newKillersKills, true];
_killer setVariable ["totalAIKills", _newKillersAIKills, true];

_killer setVariable ["playerNews", _killerNewsFeed, true];
["scripts\serverFunctions\activateNewsfeed.sqf", "BIS_fnc_execVM", _killer, false, false] call BIS_fnc_MP;

[[], "fn_hudUpdate",true,true] call BIS_fnc_MP;
