fnc_handleKill = {
	_killersXP = killer getVariable "playerXP";
	_killersKills = killer getVariable "playerKills";
	_killersTotalKills = killer getVariable "totalPlayerKills";
	_killersStreak = killer getVariable "killStreak";
	_killersLargestKillStreak = killer getVariable "largestKillStreak";
	_killersLongestKill = killer getVariable "longestPlayerKill";
	_killerNewsFeed = killer getVariable "playerNews";

	_playerDeaths = killedPlayer getVariable "playerDeaths";
	_playerTotalDeaths = killedPlayer getVariable "deathsFromPlayers";

	_newKillersKills = _killersKills + 1;
	_newTotalKills = _killersTotalKills + 1;
	_newKillStreak = _killersStreak + 1;

	_xpForKill = 100 + (_newKillStreak * 50);
	_newXP = _killersXP + _xpForKill;
	_killFeed = format ["[ %1+ ] You killed %2", _xpForKill, name killedPlayer];
	_killerNewsFeed pushBack [_killFeed, 10];

	_newPlayerDeaths = _playerDeaths + 1;
	_newTotalPlayerDeaths = _playerTotalDeaths + 1;

	killer setVariable ["playerXP", _newXP, true];
	killer setVariable ["playerKills", _newKillersKills, true];
	killer setVariable ["totalPlayerKills", _newTotalKills, true];
	killer setVariable ["killStreak", _newKillStreak, true];
	if (_newKillStreak > _killersLargestKillStreak) then { killer setVariable ["largestKillStreak", _newKillStreak, true]; };

	_killDistance = killer distance killedPlayer;
	if (_killDistance > _killersLongestKill) then { killer setVariable ["longestPlayerKill", _killDistance, true]; };

	killedPlayer setVariable ["playerDeaths", _newPlayerDeaths, true];
	killedPlayer setVariable ["deathsFromPlayers", _playerTotalDeaths, true];
	killedPlayer setVariable ["killStreak", 0, true];
	killedPlayer setVariable ["ammoCrateUsed", false, true];
	killedPlayer setVariable ["uavUsed", false, true];
	killedPlayer setVariable ["mortarStrikeUsed", false, true];

	killer setVariable ["playerNews", _killerNewsFeed, true];
	["scripts\serverFunctions\activateNewsfeed.sqf","BIS_fnc_execVM", killer, false, false] call BIS_fnc_MP;

	[[], "fn_hudUpdate",true,true ] call BIS_fnc_MP;
};

fnc_handleAIKill = {
	_playerDeaths = killedPlayer getVariable "playerDeaths";
	_deathsFromAI = killedPlayer getVariable "deathsFromAI";

	_newPlayerDeaths = _playerDeaths + 1;
	_newDeathsFromAI = _deathsFromAI + 1;

	killedPlayer setVariable ["playerDeaths", _newPlayerDeaths, true];
	killedPlayer setVariable ["deathsFromAI", _newDeathsFromAI, true];
	killedPlayer setVariable ["killStreak", 0, true];
	killedPlayer setVariable ["ammoCrateUsed", false, true];
	killedPlayer setVariable ["uavUsed", false, true];
	killedPlayer setVariable ["mortarStrikeUsed", false, true];
};

fnc_handleFriendlyKill = {
	_killersXP = killer getVariable "playerXP";
	_killersFK = killer getVariable "friendlyKills";
	_killersTotalFK = killer getVariable "totalFriendlyKills";

	_xpForKill = -50;

	if (_killersXP <= 0) then { _xpForKill = 0; };

	killer setVariable ["playerXP", _xpForKill, true];

	_killersNewFK = _killersFK + 1;
	_killersNewTotalFK = _killersTotalFK + 1;

	killer setVariable ["friendlyKills", _killersNewFK, true];
	killer setVariable ["totalFriendlyKills", _killersNewTotalFK, true];
};

aliveTo = time;
_totalSecondsAlive = player getVariable "aliveTime";
_secondsAlive = aliveTo - aliveFrom;
_totalSecondsAlive = _totalSecondsAlive + _secondsAlive;
player setVariable ["aliveTime", _totalSecondsAlive, true];

_friendlyKill = false;

killedPlayer = _this select 0;
if (isNull (_this select 2)) then {
	killer = _this select 1;
} else {
	killer = _this select 2;
};

killer = _this select 1;

killerSide = side killer;
killedPlayerSide = playerSide;

// Suicide
if (killedPlayer == killer) exitWith {};

// Friendly Kill
if (killedPlayerSide == killerSide) then {_friendlyKill = true};
if (_friendlyKill) then { [] spawn fnc_handleFriendlyKill; };

// Human Kill
if (!_friendlyKill && (killerSide == WEST || killerSide == EAST)) then { [] spawn fnc_handleKill; };

// AI Kill
if (!_friendlyKill && (killerSide == independent)) then { [] spawn fnc_handleAIKill; };
