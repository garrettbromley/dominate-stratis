private ["_timeForGod"]

if (vehicle player == player) then {
  cutText ["Put your cursor over the vehicle you wish to give God Mode.", "PLAIN"];

  _timeForGod = 3;
  hint format ["Please wait %1 seconds for vehicle to be given God Mode.", _timeForGod];
  sleep _timeForGod;

  cursorTarget allowDamage false;
  cursorTarget addeventhandler ["fired", {(_this select 0) setvehicleammo 1}];
} else {
  vehicle player allowDamage false;
  vehicle player addeventhandler ["fired", {(_this select 0) setvehicleammo 1}];
}
