cutText ["Put your cursor over the vehicle you wish to delete.", "PLAIN"];

_timeForDelete = 5;
hint format ["Please wait %1 seconds for vehicle to be deleted.",_timeForDelete];
sleep _timeForDelete;

deleteVehicle cursorTarget;
