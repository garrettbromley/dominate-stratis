#include "\A3\Ui_f\hpp\defineResinclDesign.inc"

if ( hasInterface ) then {

	{
		uiNamespace setVariable[ _x, missionNamespace getVariable _x ];
	}forEach [
		"arsenalFunctions_fnc_overrideVAButtonDown",
		"arsenalFunctions_fnc_overrideVATemplateOK",
		"arsenalFunctions_fnc_loadInventory_whiteList"
	];

	[ missionNamespace, "arsenalOpened", {
	    disableSerialization;
	    _display = _this select 0;

		waitUntil { !isNil "BIS_fnc_arsenal_target" };

		_center = BIS_fnc_arsenal_center;
		_cargo = BIS_fnc_arsenal_cargo;

		_virtualItemCargo =
			(missionNamespace call BIS_fnc_getVirtualItemCargo) +
			(_cargo call BIS_fnc_getVirtualItemCargo) +
			items _center +
			assignedItems _center +
			primaryWeaponItems _center +
			secondaryWeaponItems _center +
			handgunItems _center +
			[uniform _center,vest _center,headgear _center,goggles _center];

		_virtualWeaponCargo = [];
		{
			_weapon = _x call BIS_fnc_baseWeapon;
			_virtualWeaponCargo set [count _virtualWeaponCargo,_weapon];
			{
				private ["_item"];
				_item = getText (_x >> "item");
				if !(_item in _virtualItemCargo) then {_virtualItemCargo set [count _virtualItemCargo,_item];};
			} forEach ((configFile >> "cfgweapons" >> _x >> "linkeditems") call BIS_fnc_returnChildren);
		} forEach ((missionNamespace call BIS_fnc_getVirtualWeaponCargo) + (_cargo call BIS_fnc_getVirtualWeaponCargo) + weapons _center + [binocular _center]);

		_virtualMagazineCargo = (missionNamespace call BIS_fnc_getVirtualMagazineCargo) + (_cargo call BIS_fnc_getVirtualMagazineCargo) + magazines _center;
		_virtualBackpackCargo = (missionNamespace call BIS_fnc_getVirtualBackpackCargo) + (_cargo call BIS_fnc_getVirtualBackpackCargo) + [backpack _center];

		_virtualCargo = [];
		{
			{
				_nul = _virtualCargo pushBack ( toLower _x );
			}forEach _x;
		}forEach [
			_virtualItemCargo,
			_virtualWeaponCargo,
			_virtualMagazineCargo,
			_virtualBackpackCargo
		];

		uiNamespace setVariable [ "arsenalFunctions_override_virtualCargo", _virtualCargo ];

		//VA template button OK
		_ctrlTemplateButtonOK = _display displayCtrl IDC_RSCDISPLAYARSENAL_TEMPLATE_BUTTONOK;
		_ctrlTemplateButtonOK ctrlRemoveAllEventHandlers "buttonclick";
		_ctrlTemplateButtonOK ctrlAddEventHandler ["buttonclick", "with uinamespace do {[ctrlparent (_this select 0)] call arsenalFunctions_fnc_overrideVATemplateOK;};"];

		//VA template listbox DblClick
		_ctrlTemplateValue = _display displayCtrl IDC_RSCDISPLAYARSENAL_TEMPLATE_VALUENAME;
		_ctrlTemplateValue ctrlRemoveAllEventHandlers "lbdblclick";
		_ctrlTemplateValue ctrlAddEventHandler ["lbdblclick", "with uinamespace do {[ctrlparent (_this select 0)] call arsenalFunctions_fnc_overrideVATemplateOK;};"];

		//VA button down, needed to override ENTER on template listbox
		_display displayAddEventHandler ["keydown", "with (uinamespace) do {_this call arsenalFunctions_fnc_overrideVAButtonDown;};"];

	} ] call BIS_fnc_addScriptedEventHandler;
};
