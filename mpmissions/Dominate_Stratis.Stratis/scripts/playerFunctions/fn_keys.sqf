disableSerialization;

private ["_D", "_Key", "_Shift", "_Ctrl", "_Alt", "_Handled", "_Default_Earplugs_Toggle_Key", "_Default_Earplugs_Increase_Volume_Key", "_Default_Earplugs_Decrease_Volume_Key", "_Default_Backpack_Toggle_Access_Key"];

params [
	["_D", objNull],
	["_Key", objNull],
	["_Shift", objNull],
	["_Ctrl", objNull],
	["_Alt", objNull]
];

_Handled = false;

_Default_Earplugs_Toggle_Key = if (count (actionKeys "User1") == 0) then {59} else {(actionKeys "User1") select 0};
_Default_Earplugs_Increase_Volume_Key = if (count (actionKeys "User2") == 0) then {60} else {(actionKeys "Ueer2") select 0};
_Default_Earplugs_Decrease_Volume_Key = if (count (actionKeys "User3") == 0) then {61} else {(actionKeys "User3") select 0};
_Default_Backpack_Toggle_Access_Key = if (count (actionKeys "User4") == 0) then {62} else {(actionKeys "User4") select 0};

// Earplugs Insert/Remove Key
if (_Key == _Default_Earplugs_Toggle_Key) then {
	if (soundVolume > 0.5) then {
		0.3 fadeSound 0.5;
		titleText ["Earplugs Inserted.", "PLAIN DOWN"];
	} else {
		if (soundVolume <= 0.5) then {
			0.3 fadeSound 1.0;
			titleText ["Earplugs Removed.", "PLAIN DOWN"];
		};
	};
};

// Earplugs Increase Volume Key
if (_Key == _Default_Earplugs_Increase_Volume_Key) then {
	if (soundVolume == 1) exitWith {
		titleText ["Earplugs highest volume reached.", "PLAIN DOWN"];
	};
	0.3 fadeSound (soundVolume + 0.10);
	titleText [format ["Earplugs volume increased to %1%2.", Earplugs_Volume, "%"], "PLAIN DOWN"];
};

// Earplugs Decrease Volume Key
if (_Key == _Default_Earplugs_Decrease_Volume_Key) then {
	if (soundVolume == 0) exitWith {
		titleText ["Earplugs lowest volume reached.", "PLAIN DOWN"];
	};
	0.3 fadeSound (soundVolume - 0.10);
	titleText [format ["Earplugs volume decreased to %1%2.", Earplugs_Volume, "%"], "PLAIN DOWN"];
};

// Backpack Toggle Access Key
if (_Key == _Default_Backpack_Toggle_Access_Key) then {
	if (player getVariable ["Backpack_Allow_Access", false]) then {
		player setVariable ["Backpack_Allow_Access", true, true];
	} else {
		player setVariable ["Backpack_Allow_Access", false, true];
	};
};

_Handled
