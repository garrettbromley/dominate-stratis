_onScreenTime = 8;

_role1 = "Welcome to";
_role1names = ["Dominate Stratis"];
_role2 = "A NEW PvP Multiplayer";
_role2names = ["Game mode..."];
_role3 = "Rank Progression, Kill Streaks,";
_role3names = ["Weapon Unlocks and Database Saves."];
_role4 = "Most actions are completed";
_role4names = ["via the Scroll Menu"];
_role5 = "See how you rank against the rest on:";
_role5names = ["DominateStratis.ArmaHosts.com"];
_role6 = "Developer and Admin";
_role6names = ["Black Saber XI"];

_roles =
[
	[_role1, _role1names],
	[_role2, _role2names],
	[_role3, _role3names],
	[_role4, _role4names],
	[_role5, _role5names],
	[_role6, _role6names]
];

{
	sleep 2;
	_memberFunction = _x select 0;
	_memberNames = _x select 1;
	_finalText = format ["<t size='0.55' color='#FF0000' align='right'>%1<br /></t>", _memberFunction];
	_finalText = _finalText + "<t size='0.70' color='#FFFFFF' align='right'>";
	{_finalText = _finalText + format ["%1<br />", _x]} forEach _memberNames;
	_finalText = _finalText + "</t>";
	_onScreenTime + (((count _memberNames) - 1) * 0.9);

	[
		_finalText,
		[safezoneX + safezoneW - 2.3,0.50],
		[safezoneY + safezoneH - 0.8,0.7],
		_onScreenTime,
		0.5
	] spawn BIS_fnc_dynamicText;

	sleep (_onScreenTime);
} forEach _roles;
