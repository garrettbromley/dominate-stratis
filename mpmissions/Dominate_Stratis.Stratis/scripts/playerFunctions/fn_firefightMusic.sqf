firefightMusic = {
	private ["_locations", "_curPos", "_closestTo", "_distanceTo", "_songList", "_songToPlay"];
	
	_songList = ["LeadTrack01b_F_EXP", "LeadTrack02_F_EXP", "LeadTrack04_F_EXP"];
	_songToPlay = _songList call BIS_fnc_selectRandom;
	
	// Remove the event handler
	player removeEventHandler ["FiredNear", firefightEH];
	
	// Check if they are close to a location
	_locations = ["range", "killFarm", "baldy", "rogaine", "mike26", "maxwell", "tempest"];
	_curPos = getPos player;
	
	_closestTo = player distance2D (getMarkerPos (_locations select 0));
	{
		_distanceTo = player distance2D (getMarkerPos (_x));
		if (_distanceTo < _closestTo) then {
			_closestTo = _distanceTo;
		}
	} forEach _locations;
	
	// If they are within 150m of the location, play the music
	if (_closestTo <= 150) then {
		[_songToPlay] call playerFunctions_fnc_playMusic;
		waitUntil { !musicPlaying };
		sleep 120;	// Cooldown
		firefightEH = player addEventHandler ["FiredNear", { [] spawn firefightMusic; }];
	} else {
		firefightEH = player addEventHandler ["FiredNear", { [] spawn firefightMusic; }];
	};
};

firefightEH = player addEventHandler ["FiredNear", { [] spawn firefightMusic; }];