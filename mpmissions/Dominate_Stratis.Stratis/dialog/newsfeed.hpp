class newsfeed
{
	idd = -1;
	onLoad = "uiNamespace setVariable ['newsfeed', _this select 0]";
	onUnload = "uiNamespace setVariable ['newsfeed', objNull]";
	onDestroy = "uiNamespace setVariable ['newsfeed', objNull]";
	fadein = 0;
	fadeout = 0;
	duration = 10e10;
	movingEnable = 0;
	controlsBackground[] = {};
	objects[] = {};

	class controls
	{
		class newsfeed : RscStructuredTextDS
		{
			idc = 1100;
			text = ""; //--- ToDo: Localize;
			x = 0.005 * safezoneW + safezoneX;
			y = 0.885 * safezoneH + safezoneY;
			w = 0.262969 * safezoneW;
			h = 0.099 * safezoneH;
			colorBackground[] = { 0, 0, 0, 0.5 };
			font = "PuristaSemibold";
			size = 0.03;
			style = ST_MULTI;
			lineSpacing = 1;
			type = 13;
			class Attributes {
				align = "left";
			};
		};
	};
};