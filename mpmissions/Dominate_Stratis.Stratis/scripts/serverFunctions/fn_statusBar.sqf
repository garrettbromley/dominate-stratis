waitUntil {!(isNull (findDisplay 46))};
disableSerialization;

_rscLayer = "osefStatusBar" call BIS_fnc_rscLayer;
_rscLayer cutRsc["osefStatusBar", "PLAIN"];
systemChat format["Loading game info...", _rscLayer];

[] spawn {
	sleep 5;
	_counter = 180;
	_timeSinceLastUpdate = 0;
	while {true} do
	{
		sleep 1;
		_counter = _counter - 1;
		((uiNamespace getVariable "osefStatusBar")displayCtrl 1000)ctrlSetText format["FPS: %1 | BLUFOR: %2 | OPFOR: %3 | GRIDREF: %4", round diag_fps, west countSide playableUnits, east countSide playableUnits,mapGridPosition player, _counter];
	};
};
