aiFunctions_heliTransport_inHeli = {
	private ["_toReturn","_heli","_grp"];
	
	_toReturn = true;
	_heli = _this select 0;
	_grp = _this select 1;
	
	{
		if !(_x in vehicle _heli) { _toReturn = false; };
	} forEach units _grp;
	
	_toReturn;
};

aiFunctions_heliTransport_wait = {
	private ["_engage", "_capturedPoints", "_independantPoints", "_helicopter"];
	
	_helicopter = _this select 0;
	
	// Sleep until one of the points are lost by the AI
	_wait = true;
	while {_wait} do {
		_capturedPoints = missionnamespace getvariable ["BIS_fnc_moduleSector_sideSectors",[-1,-1,-1,-1,-1]];
		_independantPoints = _capturedPoints select 2;
			
		if (_independantPoints < 7) then {
			_wait = false;
			diag_log format ["Engaging %1", _helicopter];
			[_helicopter] spawn aiFunctions_heliTransport_engage;
		} else {
			sleep 10;
		};
	};
};

aiFunctions_heliTransport_closestGroup = {
	private ["_aliveGroups", "_spawn", "_closestGroup", "_distanceTo"];
	
	_returnValue = grpNull;

	_aliveGroups = aiAliveGroups;
	_currentlyTransported = aiAliveGroupsTransported;
	_grps = _aliveGroups - _currentlyTransported;
	if (count _grps == 0) exitWith { _returnValue };
	_spawn = getMarkerPos "AISpawn";
	_closestGroup = _grps select 0;
	_closestDistance = leader (_grps select 0) distance2D _spawn;
	{
		_distanceTo = leader _x distance2D _spawn;
		if (_distanceTo < _closestDistance) then {
			_closestDistance = _distanceTo;
			_closestGroup = _x;
		}
	} forEach _grps;
	
	if ((leader _closestGroup distance2D _spawn) <= 1000) then {
		_returnValue = _closestGroup;
	};
	
	_returnValue;
};

aiFunctions_heliTransport_closestSector = {
	private ["_locationNames", "_sectors", "_notControlled", "_spawn", "_closestDistance", "_closestSector", "_distanceTo"];
	
	// Find objectives not controlled by AI
	_locationNames = ["range", "killFarm", "baldy", "rogaine", "mike26", "maxwell", "tempest"];
	_sectors = [rangeOwner, killFarmOwner, baldyOwner, rogaineOwner, mike26Owner, maxwellOwner, tempestOwner];
	_notControlled = [];
	{
		if (_x != "GUER") then {
			_notControlled pushBack (_locationNames select _forEachIndex);
		};
	} forEach _sectors;
	
	// Get the closest not controlled point
	_spawn = getMarkerPos "AISpawn";
	_closestDistance = _spawn distance2D (getMarkerPos (_notControlled select 0));
	_closestSector = _notControlled select 0;
	{
		_distanceTo = _spawn distance2D (getMarkerPos (_x));
		if (_distanceTo < _closestDistance) then {
			_closestDistance = _distanceTo;
			_closestSector = _x;
		}
	} forEach _notControlled;
	
	_closestSector;
};

aiFunctions_heliTransport_engage = {
	private ["_theAO", "_theGroup", "_theLZ", "_helicopter", "_pilot"];
	
	_helicopter = _this select 0;
	
	// Spawn the pilot and copilot
	createVehicleCrew _helicopter;
	_pilot = driver _helicopter;
	{_x addEventHandler ["Killed", {[_this select 0, _this select 1] execVM "scripts\serverFunctions\aiKilled.sqf";}];} forEach crew _helicopter;
	
	diag_log "Crew Created";
	
	// Run the loops
	while {alive _helicopter} do {
		// Send the helicopter to pickup point
		deleteWaypoint [group _pilot, currentWaypoint group _pilot];

		_wpRTB = group _pilot addWaypoint [getMarkerPos "AISpawn", 0];
		_wpRTB setWaypointType "MOVE";
		_wpRTB setWaypointStatements ["TRUE", "vehicle this land ""GET IN"""];
		_wpRTB setWaypointBehaviour "CARELESS";
		
		_theGroup = grpNull;
		
		// Find the closest group to the spawn
		while {isNull _theGroup} do { _theGroup = [] call aiFunctions_heliTransport_closestGroup; };
		diag_log format ["Assigning %1 to %2", _theGroup, _helicopter];
		aiAliveGroupsTransported pushBack _theGroup;
		
		// Find a good LZ close to the group
		_theLZ = getMarkerPos "AISpawn";
		diag_log format ["LZ for Pickup: %1", _theLZ];
		
		// Set the waypoint to land at the group
		deleteWaypoint [group _pilot, currentWaypoint group _pilot];

		_wpLand = group _pilot addWaypoint [_theLZ, 0];
		_wpLand setWaypointType "MOVE";
		_wpLand setWaypointStatements ["TRUE", "vehicle this land ""GET IN"""];
		_wpLand setWaypointBehaviour "CARELESS";
		
		// Wait until they take off
		waitUntil {((getPos _helicopter) select 2) > 10 || (!alive _pilot) || (!canMove _helicopter) || (fuel _helicopter == 0) };
		diag_log format ["%1 has taken off...", _helicopter];
		// Wait until they land
		waitUntil {(((getPos _helicopter) select 2) < 5 && _helicopter distance2D _theLZ < 50) || (!alive _pilot)  || (!canMove _helicopter) || (fuel _helicopter == 0) };
		diag_log format ["%1 has landed...", _helicopter];
		
		if (!alive _pilot || !canMove _helicopter || fuel _helicopter == 0) exitWith { [_helicopter] execVM "@server\sendCas.sqf"; };
		//if (!alive _pilot) exitWith { [_helicopter] spawn { sleep 10; (_this select 0) setDamage 1; }; };
		
		// Tell the group to get in the vehicle
		{_x assignAsCargo _helicopter} foreach units _theGroup; 
		{[_x] orderGetIn true} foreach units _theGroup;
		
		// Wait until the group is in the vehicle
		waitUntil { [_helicopter, _theGroup] call aiFunctions_heliTransport_inHeli || (_theGroup isEqualTo grpNull)};
		if (_theGroup isEqualTo grpNull) exitWith { 
			diag_log "Group Gone, Adjusting...";  
			[_helicopter] spawn aiFunctions_heliTransport;
		};
		diag_log format ["%1 is in the chopper [%2]", _theGroup, _helicopter];
		
		if (!alive _pilot || !canMove _helicopter || fuel _helicopter == 0) exitWith { [_helicopter] execVM "@server\sendCas.sqf"; };
		//if (!alive _pilot) exitWith { [_helicopter] spawn { sleep 10; (_this select 0) setDamage 1; }; };
		
		// Find the closest enemy sector to the squad
		_theAO = [] call aiFunctions_heliTransport_closestSector;
		diag_log format ["%1 is headed to %2...", _theGroup, _theAO];
			
		// LZ for closest enemy sector		
		_theLZ = [getMarkerPos _theAO, 25, 200, 5, 0, 1, 0] call BIS_fnc_findSafePos;
		
		// Set waypoint to move to the LZ
		_wpMove = group _pilot addWaypoint [_theLZ, 0];
		_wpMove setWaypointType "MOVE";
		_wpMove setWaypointBehaviour "CARELESS";
		
		diag_log format ["%1 is moving to %2 for insertion...", _helicopter, _theLZ];

		// Wait until they are at the LZ
		waitUntil {_helicopter distance2D _theLZ < 100 || (!alive _pilot) || (!canMove _helicopter) || (fuel _helicopter == 0) };
		diag_log format ["%1 has transported %2 to the LZ...", _helicopter, _theGroup];
		
		if (!alive _pilot || !canMove _helicopter || fuel _helicopter == 0) exitWith { [_helicopter] execVM "@server\sendCas.sqf"; };
		//if (!alive _pilot) exitWith { [_helicopter] spawn { sleep 10; (_this select 0) setDamage 1; }; };
		
		// Tell the group to rappell from the helicopter
		[_helicopter] call AR_Rappel_All_Cargo;
		
		// Wait for them to start rappelling
		waitUntil { { _x getVariable ["AR_Is_Rappelling", false] } forEach units _theGroup };
		
		// Wait for them to finish rappelling
		while { {_x getVariable ["AR_Is_Rappelling", false] } forEach units _theGroup } do {};
		aiAliveGroupsTransported = aiAliveGroupsTransported - [_theGroup];
		
		
		
		diag_log format ["%1 is on the ground...", _theGroup];
		diag_log format ["aiAliveGroups: %1", aiAliveGroups];
		diag_log format ["aiAliveGroupsTransported: %1", aiAliveGroupsTransported];
		
		if (!alive _pilot || !canMove _helicopter || fuel _helicopter == 0) exitWith { [_helicopter] execVM "@server\sendCas.sqf"; };
		//if (!alive _pilot) exitWith { [_helicopter] spawn { sleep 10; (_this select 0) setDamage 1; }; };
	};
};

aiFunctions_heliTransport = {
	private ["_helicopter"];

	_helicopter = _this select 0;
	diag_log format ["heliTransport added to: %1", _helicopter];
	[_helicopter] spawn aiFunctions_heliTransport_wait;
};