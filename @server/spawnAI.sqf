// Spawn Units
// Expression: {_x addEventHandler ["Killed", {[_this select 0, _this select 1] execVM "scripts\serverFunctions\aiKilled.sqf";}];} forEach units (_this select 0);

private ["_manpowerCap", "_spawnRate", "_maxVehicles", "_maxSoldiers", "_deleteCorpses", "_deleteWrecks", "_spawnPoint", "_startMainLoop", "_capturedPoints", "_independantPoints"];


// SETTINGS
_manpowerCap = 50;
_spawnRate = 120;
_maxVehicles = 1;
_vehicleTypes = ["I_G_Offroad_01_F", "I_G_Van_01_transport_F"];
_chanceOfVehicle = 20;
_maxSoldiers = 4;
_deleteCorpses = 30;
_deleteWrecks = 45;
_spawnPoint = "AIspawn";
_vehicleSpawn = "AIvehicleSpawn";

// Sleep until one of the points are lost by the AI
_startMainLoop = false;
while {_startMainLoop == false} do {
	_capturedPoints = missionnamespace getvariable ["BIS_fnc_moduleSector_sideSectors",[-1,-1,-1,-1,-1]];
	_independantPoints = _capturedPoints select 2;
	
	if (_independantPoints < 5) then {
		_startMainLoop = true;
	}
	
	sleep 10;
};


// Begin main loop
while {true} do {
	// Find objectives not controlled by AI
	_locationNames = ["range", "killFarm", "baldy", "rogaine", "mike26", "maxwell", "tempest"];
	_sectors = [rangeOwner, killFarmOwner, baldyOwner, baldyOwner, rogaineOwner, mike26Owner, maxwellOwner, tempestOwner];
	_notControlled = [];
	{
		if (_x != "GUER") then {
			_notControlled pushBack (_locationNames select _forEachIndex);
		};
	} forEach _sectors;
	
	// If there are points that aren't controlled, then spawn support
	if (count _notControlled > 0) then {
		// Spawn the group	
		_AIgroup = [getMarkerPos _spawnPoint, independent, 5] call BIS_fnc_spawnGroup;
		
		// Initialize the members of the group
		{
			_x addEventHandler ["Killed", {[_this select 0, _this select 1] execVM "scripts\serverFunctions\aiKilled.sqf";}];
		} forEach units _AIgroup;
		
		// Spawn an offroad
		_chance = random 100;
		if (_chance <= _chanceOfVehicle) then { _spawnVehicle = true; };
		
		if (_spawnVehicle) then {
			_vehClass = [_vehicleTypes] call BIS_fnc_selectRandom;
			_veh = createVehicle getMarkerPos _vehicleSpawn;
			{_x moveInCargo _veh} forEach units _AIgroup; 
			leader _AIgroup assignAsDriver _veh;
		};
		
		// Get the closest not controlled point
		_closestSector = "";
		{
	
		} forEach _notControlled;
	};
	
	
	sleep _spawnRate;
};