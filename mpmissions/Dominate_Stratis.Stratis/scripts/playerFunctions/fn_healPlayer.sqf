0 = player addEventHandler ["HandleHeal", {
	_this spawn {
		_injured = _this select 0;
		_damage = damage _injured;
		waitUntil {damage _injured != _damage};
		_injured setDamage 0;
	};
}];
