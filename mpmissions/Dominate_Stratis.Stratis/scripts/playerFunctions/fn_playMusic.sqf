private ["_song"];

// Get the inputs
_song = _this select 0;

playMusic _song;
musicPlaying = true;
musicEH = addMusicEventHandler ["MusicStop", { musicPlaying = false; removeMusicEventHandler ["MusicStop", musicEH]; musicEH = -1; }];