/*
	File: initServer.sqf
	Author: Garrett Bromley
	Website: ArmaHosts.com
	Last Edited: 12-19-2016

	Using this code without ArmaHosts's direct permission is forbidden

*/

private ["_databaseName"];

_databaseName = "a3Storm";
diag_log format ["Database: %1", _databaseName];		//Log the database name
[_databaseName, "SQL_CUSTOM", "a3Storm"] execVM "database\extDB\init.sqf";
diag_log "Database Being Connected";	//Log the init

Storm_fnc_checkPlayer = compile preprocessFile "database\checkPlayer.sqf";
Storm_fnc_getPlayerData = compile preprocessFile "database\getPlayerData.sqf";
Storm_fnc_setPlayerData = compile preprocessFile "database\setPlayerData.sqf";
Storm_fnc_insertPlayer = compile preprocessFile "database\insertPlayer.sqf";
Storm_fnc_getPlayerGear = compile preprocessFile "database\getPlayerGear.sqf";
Storm_fnc_setPlayerGear = compile preprocessFile "database\setPlayerGear.sqf";

[] execVM "@admin\adminConfig.sqf";
[] execVM "@server\serverRankConfig.sqf";

["Initialize"] call BIS_fnc_dynamicGroups;


waitUntil {time > 0};
// Time Settings
_timeToSkip = floor random 25;
skipTime _timeToSkip;
setTimeMultiplier 20;

// Variables for holding who has points
rangeOwner 		= "GUER"; publicVariable "rangeOwner";
killFarmOwner 	= "GUER"; publicVariable "killFarmOwner";
baldyOwner 		= "GUER"; publicVariable "baldyOwner";
rogaineOwner 	= "GUER"; publicVariable "rogaineOwner";
mike26Owner 	= "GUER"; publicVariable "mike26Owner";
maxwellOwner 	= "GUER"; publicVariable "maxwellOwner";
tempestOwner 	= "GUER"; publicVariable "tempestOwner";

[] execVM "@server\monitorGroups.sqf";
heli_transport_functions = [] execVM "@server\heliTransport.sqf";
waitUntil { scriptDone heli_transport_functions };
[aiHeli1] spawn aiFunctions_heliTransport;
aiHeli1 addEventHandler ["Killed", {[_this select 1, _this select 2, 200] execVM "scripts\serverFunctions\vehicleKilled.sqf";}];
//[aiHeli2] spawn aiFunctions_heliTransport;

// Killfeed variables to avoid errors
killfeed_switch = 1; // 1 = ON, 0 = OFF. DEFAULT: 1
killfeed_loop_delay = 1; // Time in seconds between checks. Don't go too high. DEFAULT: 1
killfeed_sides_player = [true,true,true,true]; // Changes what sides work with killfeed for Players. [West,East,Independent,Civilian]. DEFAULT: true,true,true,true
killfeed_sides_ai = [true,true,true,true]; // Changes what sides work with killfeed for AI. [West,East,Independent,Civilian]. DEFAULT: true,true,true,true
killfeed_board_time = 6; // How many seconds one name can be on the board. DEFAULT: 6