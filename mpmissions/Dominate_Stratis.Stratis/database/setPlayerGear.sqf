if (!isServer) exitWith {};

private ["_UID", "_gearToSet", "_side"];

_UID = _this select 0;
_gearToSet = _this select 1;
_side = _this select 2;

if (_side == "WEST") then {
	[format ["setPlayerGearWEST:%1:%2", _UID, _gearToSet], 2] call extDB_fnc_async;
};

if (_side == "EAST") then {
	[format ["setPlayerGearEAST:%1:%2", _UID, _gearToSet], 2] call extDB_fnc_async;
};
