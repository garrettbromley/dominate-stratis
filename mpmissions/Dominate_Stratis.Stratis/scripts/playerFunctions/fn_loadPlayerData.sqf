private ["_playerExists"];

myName = name player;
myUID = getPlayerUID player;
myUnit = player;
mySide = side player;

if (mySide == WEST) then {
	mySideString = "WEST";
};

if (mySide == EAST) then {
	mySideString = "EAST";
};

[[myUID, myUnit], "Storm_fnc_checkPlayer", false, false, true] call BIS_fnc_MP;

_playerExists = nil;

while {isNil "_playerExists"} do {
	_playerExists = myUnit getVariable "playerExists";
	sleep 1;
};

if (_playerExists) then {
	[[myUID, myUnit], "Storm_fnc_getPlayerData", false, false, true] call BIS_fnc_MP;
	[[myUID, myUnit, mySideString], "Storm_fnc_getPlayerGear", false, false, true] call BIS_fnc_MP;
} else {
	[[myName, myUID], "Storm_fnc_insertPlayer", false, false, true] call BIS_fnc_MP;
	sleep 5;
	[[myUID, myUnit], "Storm_fnc_getPlayerData", false, false, true] call BIS_fnc_MP;
	[[myUID, myUnit, mySideString], "Storm_fnc_getPlayerGear", false, false, true] call BIS_fnc_MP;
};

[] execVM "config\rankConfig.sqf";

proceedWithInit = true;
