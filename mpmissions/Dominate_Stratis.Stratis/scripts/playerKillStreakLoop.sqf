currentXP = player getVariable "playerXP";

while {true} do {
	private ["_killStreak", "_ammoCrateUsed", "_uavUsed", "_mortarStrikeUsed"];

	sleep 5;

	_killStreak = player getVariable "killStreak";

	_ammoCrateUsed = player getVariable "ammoCrateUsed";
	_uavUsed = player getVariable "uavUsed";
	_mortarStrikeUsed = player getVariable "mortarStrikeUsed";

	if (_killStreak >= 5) then {
		if (isNil "ammoCrateReward" && !_ammoCrateUsed) then {
			ammoCrateReward = player addaction [("<t color=""#00FF00"">" + ("REQUEST SUPPLY DROP") +"</t>"),"scripts\killStreaks\ammoCrate.sqf", "",5,false,true,"", ""];
			_newsFeed = player getVariable "playerNews";
			_streakUpdate = "Ammo Crate Drop now available!";
			_newsFeed pushBack [_streakUpdate, 10];
			player setVariable ["playerNews", _newsFeed, true];
			[] execVM "scripts\serverFunctions\activateNewsfeed.sqf";
		};
	};

	if (_killStreak >= 10) then {
		if (isNil "uavReward" && !_uavUsed) then {
			uavReward = player addaction [("<t color=""#00FF00"">" + ("DEPLOY UAV") +"</t>"),"scripts\killStreaks\uav.sqf", "",5,false,true,"", ""];
			_newsFeed = player getVariable "playerNews";
			_streakUpdate = "UAV Ready to be deployed!";
			_newsFeed pushBack [_streakUpdate, 10];
			player setVariable ["playerNews", _newsFeed, true];
			[] execVM "scripts\serverFunctions\activateNewsfeed.sqf";
		};
	};

	if (_killStreak >= 15) then {
		if (isNil "mortarStrikeReward" && !_mortarStrikeUsed) then {
			mortarStrikeReward = player addaction [("<t color=""#00FF00"">" + ("REQUEST MORTAR STRIKE") +"</t>"),"scripts\killStreaks\mortarStrike.sqf", "",5,false,true,"", ""];
			_newsFeed = player getVariable "playerNews";
			_streakUpdate = "Mortar Strike now available!";
			_newsFeed pushBack [_streakUpdate, 10];
			player setVariable ["playerNews", _newsFeed, true];
			[] execVM "scripts\serverFunctions\activateNewsfeed.sqf";
		};
	};
};
