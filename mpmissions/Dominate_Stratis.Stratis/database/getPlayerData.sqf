if (!isServer) exitWith {};

private ["_UID", "_playerUnit", "_xp", "_humanKills", "_friendlyKills", "_aiKills", "_vehicleKills", "_longestPlayerKill", "_longestAIKill", "_largestKillStreak", "_deathsFromPlayers", "_deathsFromAI", "_ammoCratesUsed", "_uavsUsed", "_mortarsUsed", "_capturedPoints", "_forcePointsGained", "_forcePointsContributed", "_forcePointsSpent", "_aliveTime", "_result"];

_UID = _this select 0;
_playerUnit = _this select 1;

_result = [format ["getPlayerData:%1", _UID], 2] call extDB_fnc_async;

diag_log format ["getPlayerData Result: %1", _result];

if (count _result > 0) then
{
	_xp = _result select 0;
	_humanKills = _result select 1;
	_friendlyKills = _result select 2;
	_aiKills = _result select 3;
	_vehicleKills = _result select 4;
	_longestPlayerKill = _result select 5;
	_longestAIKill = _result select 6;
	_largestKillStreak = _result select 7;
	_deathsFromPlayers = _result select 8;
	_deathsFromAI = _result select 9;
	_ammoCratesUsed = _result select 10;
	_uavsUsed = _result select 11;
	_mortarsUsed = _result select 12;
	_capturedPoints = _result select 13;
	_forcePointsGained = _result select 14;
	_forcePointsContributed = _result select 15;
	_forcePointsSpent = _result select 16;
	_aliveTime = _result select 17;
};

_playerUnit setVariable ["playerXP", _xp, true];
_playerUnit setVariable ["totalPlayerKills", _humanKills, true];
_playerUnit setVariable ["totalFriendlyKills", _friendlyKills, true];
_playerUnit setVariable ["totalAIKills", _aiKills, true];
_playerUnit setVariable ["vehicleKills", _vehicleKills, true];
_playerUnit setVariable ["longestPlayerKill", _longestPlayerKill, true];
_playerUnit setVariable ["longestAIKill", _longestAIKill, true];
_playerUnit setVariable ["largestKillStreak", _largestKillStreak, true];
_playerUnit setVariable ["deathsFromPlayers", _deathsFromPlayers, true];
_playerUnit setVariable ["deathsFromAI", _deathsFromAI, true];
_playerUnit setVariable ["ammoCratesUsed", _ammoCratesUsed, true];
_playerUnit setVariable ["uavsUsed", _uavsUsed, true];
_playerUnit setVariable ["mortarsUsed", _mortarsUsed, true];
_playerUnit setVariable ["capturedPoints", _capturedPoints, true];
_playerUnit setVariable ["forcePointsGained", _forcePointsGained, true];
_playerUnit setVariable ["forcePointsContributed", _forcePointsContributed, true];
_playerUnit setVariable ["forcePointsSpent", _forcePointsSpent, true];
_playerUnit setVariable ["aliveTime", _aliveTime, true];
