private ["_playersHealth"];

playerInPlayerArea = false;

while {!playerInPlayerArea} do
{
	if (!alive player) exitWith {};
	titleText ["RETURN TO THE PLAYER AREA IMMEDIATELY. YOU WILL DIE...", "PLAIN", 3];
	systemChat "RETURN TO THE PLAYER AREA IMMEDIATELY. YOU WILL DIE...";
	hint "RETURN TO THE PLAYER AREA IMMEDIATELY. YOU WILL DIE...";
	_playersHealth = damage player;
	_playersHealth = _playersHealth - .1;
	player setDamage _playersHealth;
};
