private ["_gear", "_gearString"];

_gear = getUnitLoadout player;
_gearString = format ["%1", _gear];
player setVariable ["playerGear", _gearString, true];
