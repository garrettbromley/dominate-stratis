if (!isServer) exitWith {};

private ["_UID", "_playerUnit", "_result", "_playerExists"];

_playerExists = nil;

_UID = _this select 0;
_playerUnit = _this select 1;

_result = ([format ["checkPlayer:%1", _UID], 2] call extDB_fnc_async) select 0;

if (_result) then
{
	_playerExists = true;
} else {
	_playerExists = false;
};

_playerUnit setVariable ["playerExists", _playerExists, true];
