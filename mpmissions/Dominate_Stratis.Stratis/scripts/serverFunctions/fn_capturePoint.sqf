private ["_targetArea", "_requiredDistance", "_forcePoints"];

if (isServer) then {
	_capturedPoint = _this select 0;
	_pointLocation = format ["%1", _capturedPoint];
	_newOwner = _this select 1;
	
	switch (_pointLocation) do {
		case "range":    { rangeOwner = format["%1", _newOwner]; publicVariable "rangeOwner"; diag_log format["%1 was captured by %2", _capturedPoint, _newOwner]; };
		case "killFarm": { killFarmOwner = format["%1", _newOwner]; publicVariable "killFarmOwner"; diag_log format["%1 was captured by %2", _capturedPoint, _newOwner]; };
		case "baldy":    { baldyOwner = format["%1", _newOwner]; publicVariable "baldyOwner"; diag_log format["%1 was captured by %2", _capturedPoint, _newOwner]; };
		case "rogaine":  { rogaineOwner = format["%1", _newOwner]; publicVariable "rogaineOwner"; diag_log format["%1 was captured by %2", _capturedPoint, _newOwner]; };
		case "mike26":   { mike26Owner = format["%1", _newOwner]; publicVariable "mike26Owner"; diag_log format["%1 was captured by %2", _capturedPoint, _newOwner]; };
		case "maxwell":  { maxwellOwner = format["%1", _newOwner]; publicVariable "maxwellOwner"; diag_log format["%1 was captured by %2", _capturedPoint, _newOwner]; };
		case "tempest":  { tempestOwner = format["%1", _newOwner]; publicVariable "tempestOwner"; diag_log format["%1 was captured by %2", _capturedPoint, _newOwner]; };
	};
} else {
	if (!alive player) exitWith {};

	_capturedPoint = _this select 0;
	_pointLocation = format ["%1", _capturedPoint];
	_newOwner = _this select 1;

	if ((side player) != _newOwner) exitWith {};

	switch (_pointLocation) do {
			case "range": {_targetArea = "Military Range"; _requiredDistance = 125; _forcePoints = 25;};
			case "killFarm": {_targetArea = "Kill Farm"; _requiredDistance = 100; _forcePoints = 20;};
			case "baldy": {_targetArea = "LZ Baldy"; _requiredDistance = 100; _forcePoints = 15;};
			case "rogaine": {_targetArea = "Camp Rogaine"; _requiredDistance = 125; _forcePoints = 25;};
			case "mike26": {_targetArea = "Air Station Mike 26"; _requiredDistance = 150; _forcePoints = 30;};
			case "maxwell": {_targetArea = "Camp Maxwell"; _requiredDistance = 100; _forcePoints = 20;};
			case "tempest":  {_targetArea = "Tempest"; _requiredDistance = 150; _forcePoints = 30;};
	};

	_pointLocation = getMarkerPos _pointLocation;
	_distanceFromPoint = player distance _pointLocation;

	if (_distanceFromPoint <= _requiredDistance) then {
		_playerXP = player getVariable "playerXP";
		_playerForcePoints = player getVariable "forcePoints";
		_playerCapturedPoints = player getVariable "capturedPoints";
		_playerForcePointsGained = player getVariable "forcePointsGained";
		_playerNewsFeed = player getVariable "playerNews";

		_xpForCap = 500;
		_playerXP = _playerXP + _xpForCap;
		_playerCapturedPoints = _playerCapturedPoints + 1;
		_playerForcePoints = _playerForcePoints + _forcePoints;
		_playerForcePointsGained = _playerForcePointsGained + _forcePoints;

		player setVariable ["playerXP", _playerXP, true];
		player setVariable ["forcePoints", _playerForcePoints, true];
		player setVariable ["capturedPoints", _playerCapturedPoints, true];
		player setVariable ["forcePointsGained", _playerForcePointsGained, true];

		_feedMessage = format ["[ +%1 ] You captured %2", _xpForCap, _targetArea];
		_playerNewsFeed pushBack [_feedMessage, 10];
		player setVariable ["playerNews", _playerNewsFeed, true];
		[] execVM "scripts\serverFunctions\activateNewsfeed.sqf";
	};
};