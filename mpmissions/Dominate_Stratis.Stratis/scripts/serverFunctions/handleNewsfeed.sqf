waitUntil {!(isNull (findDisplay 46))};
disableSerialization;

_rscLayer = "newsfeed" call BIS_fnc_rscLayer;
_rscLayer cutRsc["newsfeed", "PLAIN"];

[] spawn {
	sleep 15;

	_loop = true;
	while {_loop} do
	{
		private ["_newsFeed", "_feedCount", "_curData", "_entryData", "_secLeft", "_delete", "_helperArray", "_newInformation"];

		sleep 1;

		_delete = false;

		_newsFeed = player getVariable "playerNews";
		_feedCount = count _newsFeed;

		if (_feedCount > 0) then {
			for [{_i = 0}, {_i < _feedCount}, {_i = _i + 1}] do
			{
				_curData = _newsFeed select _i;
				_entryData = _curData select 0;
				_secLeft = _curData select 1;

				if (_secLeft == 0) then {
					_delete = true;
				} else {
					_delete = false;
				};

				if (_delete) then {
					_helperArray = [];
					_helperArray pushBack _curData;
					_newsFeed = _newsFeed - _helperArray;
				} else {
					_secLeft = _secLeft - 1;
					_newsFeed set [_i, [_entryData, _secLeft]];
				};


				_feedCount = count _newsFeed;
				if (_feedCount == 0) then {
					for [{_i = 0.5}, {_i > 0}, {_i = _i - 0.01}] do
					{
						((uiNamespace getVariable "newsfeed")displayCtrl 1100)ctrlSetBackgroundColor [0, 0, 0, _i];
						((uiNamespace getVariable "newsfeed")displayCtrl 1100)ctrlSetTextColor [1, 1, 1, _i];
						sleep 0.1;
					};
				};

				player setVariable ["playerNews", _newsFeed, true];

				_newInformation = player getVariable "playerNews";
			};
		};
	};
};
