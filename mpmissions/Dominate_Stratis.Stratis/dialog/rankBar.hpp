class rankBar
{
	idd = -1;
	onLoad = "uiNamespace setVariable ['rankBar', _this select 0]";
	onUnload = "uiNamespace setVariable ['rankBar', objNull]";
	onDestroy = "uiNamespace setVariable ['rankBar', objNull]";
	fadein = 0;
	fadeout = 0;
	duration = 10e10;
	movingEnable = 0;
	controlsBackground[] = {};
	objects[] = {};

	class controls
	{
		class titleBack : IGUIBackDS
		{
			idc = 2200;
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.180469 * safezoneW;
			h = 0.022 * safezoneH;
			colorBackground[] = { 0.69, 0.75, 0.5, 1 };
		};
		class mainBack : IGUIBackDS
		{
			idc = 2201;
			x = 0.814531 * safezoneW + safezoneX;
			y = 0.863 * safezoneH + safezoneY;
			w = 0.180469 * safezoneW;
			h = 0.088 * safezoneH;
			colorBackground[] = { 0, 0, 0, 1 };
		};
		class rankIcon : RscPictureDS
		{
			idc = 1200;
			text = "";
			x = 0.819688 * safezoneW + safezoneX;
			y = 0.874 * safezoneH + safezoneY;
			w = 0.04125 * safezoneW;
			h = 0.066 * safezoneH;
		};
		class rankName : RscTextDS
		{
			idc = 1000;
			text = "Loading Rank Name..."; //--- ToDo: Localize;
			x = 0.866094 * safezoneW + safezoneX;
			y = 0.874 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.022 * safezoneH;
			font = "PuristaSemibold";
			size = 0.03;
			type = 13;
			style = 1;
			class Attributes {
				align = "left";
				color = "#FFFFFF";
			};
		};
		class rankNo : RscTextDS
		{
			idc = 1001;
			text = "Loading Rank Number..."; //--- ToDo: Localize;
			x = 0.866094 * safezoneW + safezoneX;
			y = 0.896 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.022 * safezoneH;
			font = "PuristaSemibold";
			size = 0.03;
			type = 13;
			style = 1;
			class Attributes {
				align = "left";
				color = "#FFFFFF";
			};
		};
		class xp : RscTextDS
		{
			idc = 1002;
			text = "Loading XP..."; //--- ToDo: Localize;
			x = 0.866094 * safezoneW + safezoneX;
			y = 0.918 * safezoneH + safezoneY;
			w = 0.12375 * safezoneW;
			h = 0.022 * safezoneH;
			font = "PuristaSemibold";
			size = 0.03;
			type = 13;
			style = 1;
			class Attributes {
				align = "left";
				color = "#FFFFFF";
			};
		};
		class gameStats : RscTextDS
		{
			idc = 1003;
			text = "Loading Game Statistics..."; //--- ToDo: Localize;
			x = 0.819688 * safezoneW + safezoneX;
			y = 0.841 * safezoneH + safezoneY;
			w = 0.170156 * safezoneW;
			h = 0.022 * safezoneH;
			font = "PuristaSemibold";
			size = 0.03;
			type = 13;
			style = 1;
			shadow = 1;
			class Attributes {
				align = "left";
				color = "#FFFFFF";
			};
		};
	};
};