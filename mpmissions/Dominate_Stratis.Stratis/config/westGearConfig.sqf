private ["_basicWeapons", "_basicMags", "_basicItems", "_basicPacks", "_addonWeapons", "_addonMags", "_addonItems", "_addonPacks", "_toAddWeapons", "_toAddItems", "_toAddMags", "_toAddPacks"];

_basicWeapons =
[
	"arifle_MX_F",
	"hgun_P07_F"
];

_basicMags =
[
	"30Rnd_65x39_caseless_mag",
	"16Rnd_9x21_Mag",
	"HandGrenade",
	"SmokeShell",
	"Chemlight_blue"
];

_basicItems =
[
	"U_B_CombatUniform_mcam_tshirt",
	"V_BandollierB_khk",
	"V_BandollierB_cbr",
	"V_BandollierB_rgr",
	"V_BandollierB_blk",
	"V_BandollierB_oli",
	"FirstAidKit",
	"acc_flashlight",
	"G_Lowprofile",
	"optic_Aco",
	"optic_ACO_grn",
	"optic_ACO_smg",
	"optic_ACO_grn_smg",
	"Binocular",
	"ItemMap",
	"ItemCompass",
	"ItemWatch",
	"ItemRadio",
	"ItemGPS",
	"NVGoggles",
	"NVGoggles_OPFOR",
	"NVGoggles_INDEP",
	"ToolKit"
];

_basicPacks =
[
	"B_AssaultPack_khk",
	"B_AssaultPack_dgtl",
	"B_AssaultPack_rgr",
	"B_AssaultPack_sgg",
	"B_AssaultPack_blk",
	"B_AssaultPack_cbr",
	"B_AssaultPack_mcamo"
];

_addonWeapons = [];
_addonMags =	[];
_addonItems =	[];
_addonPacks =	[];

_toAddWeapons = [];
_toAddMags =	[];
_toAddItems =	[];
_toAddPacks =	[];

playerRankNo = player getVariable "playerRankNo";

for [{_i = 0}, {_i <= playerRankNo}, {_i = _i + 1}] do
{
    switch (_i) do
	{
		case 1: {};

		case 2: {
					_toAddWeapons =	["arifle_MX_GL_F"];
					_toAddMags = ["1Rnd_HE_Grenade_shell","UGL_FlareGreen_F","UGL_FlareRed_F","1Rnd_SmokeBlue_Grenade_shell","1Rnd_SmokePurple_Grenade_shell","1Rnd_SmokeYellow_Grenade_shell","1Rnd_SmokeGreen_Grenade_shell","1Rnd_SmokeRed_Grenade_shell","1Rnd_Smoke_Grenade_shell"];
				};

		case 3: {
					_toAddItems =	["optic_Holosight_smg","bipod_03_F_blk","bipod_02_F_blk","bipod_01_F_blk","bipod_02_F_hex","bipod_01_F_mtp","bipod_03_F_oli","bipod_01_F_snd","bipod_02_F_tan"];
				};
		case 4: {
					_toAddItems =	["V_HarnessOGL_brn","V_HarnessOGL_gry"];
				};

		case 5: {
					_toAddWeapons = ["hgun_PDW2000_F"];
					_toAddMags =	["30Rnd_9x21_Mag"];
					_toAddItems =	["optic_Holosight","acc_pointer_IR"];
				};

		case 6: {
					_toAddItems =	["H_Cap_red","H_Cap_blu","H_Cap_oli","H_Cap_headphones","H_Cap_tan","H_Cap_blk","H_Cap_blk_CMMG","H_Cap_brn_SPECOPS","H_Cap_tan_specops_US","H_Cap_khaki_specops_UK","H_Cap_grn","H_Cap_grn_BI","H_Cap_blk_Raven","H_Cap_blk_ION","H_Cap_oli_hs","H_Cap_press"];
				};

		case 7: {
					_toAddWeapons =	["MMG_02_black_F","MMG_02_camo_F","MMG_02_sand_F"];
				};

		case 8: {
					_toAddWeapons = ["SMG_02_F"];
					_toAddMags =	["30Rnd_9x21_Mag"];
				};

		case 9: {
					_toAddWeapons = ["arifle_TRG21_F"];
					_toAddMags =	["30Rnd_556x45_Stanag_Tracer_Green"];
				};

		case 10: {
					_toAddWeapons = ["SMG_01_F","hgun_Rook40_F"];
					_toAddMags =	["30Rnd_45ACP_Mag_SMG_01_tracer_green","16Rnd_9x21_Mag"];
					_toAddItems =	["G_Diving","U_B_Wetsuit","V_RebreatherB"];
				};

		case 11: {
					_toAddWeapons = ["arifle_TRG21_GL_F"];
					_toAddItems =	["V_Chestrig_blk","V_Chestrig_oli"];
					_toAddPacks =	["B_FieldPack_blk","B_FieldPack_ocamo","B_FieldPack_oucamo","B_FieldPack_cbr"];
				};

		case 12: {
					_toAddWeapons = ["arifle_Mk20C_F"];
				};

		case 13: {
					_toAddItems =	["H_CrewHelmetHeli_B"];
				};

		case 14: {
					_toAddWeapons =	["arifle_Mk20_plain_F"];
				};

		case 15: {
					_toAddWeapons =	["arifle_Katiba_C_F","launch_RPG7_F"];
					_toAddMags =	["RPG7_F"];
					_toAddItems =	["optic_MRCO","V_HarnessO_brn","V_HarnessOSpec_brn","V_HarnessOSpec_gry"];
				};

		case 16: {
					_toAddWeapons = ["arifle_Mk20_GL_F"];
				};

		case 17: {
					_toAddItems =	["H_MilCap_ocamo","H_MilCap_mcamo","H_MilCap_oucamo","H_MilCap_rucamo","H_MilCap_gry","H_MilCap_dgtl","H_MilCap_blue"];
				};

		case 18: {
					_toAddWeapons = ["MMG_01_hex_F","MMG_01_tan_F"];
					_toAddItems =	["optic_DMS"];
				};

		case 19: {
					_toAddItems =	["G_Shades_Black","G_Shades_Blue","G_Shades_Green","G_Shades_Red"];
				};

		case 20: {
					_toAddWeapons = ["arifle_Katiba_F","arifle_MXC_F"];
					_toAddItems =	["optic_Hamr","V_Chestrig_khk","V_Chestrig_rgr","U_B_HeliPilotCoveralls"];
				};

		case 21: {
					_toAddWeapons =	["srifle_DMR_06_camo_F","srifle_DMR_06_olive_F"];
				};

		case 22: {
					_toAddWeapons = ["arifle_Katiba_GL_F"];
				};

		case 23: {
					_toAddItems =	["optic_SOS"];
				};

		case 24: {
					_toAddWeapons = ["launch_RPG7_F"];
					_toAddMags =	["RPG7_F"];
					_toAddItems =	["G_Combat"];
				};

		case 25: {
					_toAddWeapons = ["arifle_MXM_F"];
					_toAddMags =	["30Rnd_65x39_caseless_mag_Tracer"];
					_toAddItems =	["V_PlateCarrier2_rgr"];
					_toAddPacks =	["B_TacticalPack_blk","B_TacticalPack_rgr","B_TacticalPack_ocamo","B_TacticalPack_mcamo","B_TacticalPack_oli"];
				};

		case 26: {
					_toAddWeapons = ["hgun_ACPC2_F"];
					_toAddMags =	["9Rnd_45ACP_Mag"];
				};

		case 27: {
					_toAddItems =	["H_BandMask_blk","H_BandMask_khk","H_BandMask_reaper","H_BandMask_demon","H_Bandanna_surfer","H_Bandanna_khk","H_Bandanna_khk_hs","H_Bandanna_cbr","H_Bandanna_sgg","H_Bandanna_gry","H_Bandanna_camo","H_Bandanna_mcamo"];
				};

		case 28: {
					_toAddItems =	["V_PlateCarrierSpec_blk","V_PlateCarrierSpec_rgr","V_PlateCarrierSpec_mtp"];
				};

		case 29: {
					_toAddItems =	["H_Watchcap_blk","H_Watchcap_khk","H_Watchcap_camo","H_Watchcap_sgg"];
				};

		case 30: {
					_toAddWeapons = ["srifle_DMR_01_F"];
					_toAddMags =	["10Rnd_762x51_Mag"];
					_toAddItems =	["Rangefinder","U_I_G_Story_Protagonist_F"];
				};

		case 31: {
					_toAddItems =	["optic_AMS","optic_AMS_khk","optic_AMS_snd","optic_AMS_old"];
				};

		case 32: {
					_toAddWeapons = ["arifle_MX_SW_F"];
					_toAddMags =	["100Rnd_65x39_caseless_mag_Tracer"];
				};

		case 33: {
					_toAddItems =	["G_Bandanna_aviator","G_Bandanna_beast","G_Bandanna_blk","G_Bandanna_khk","G_Bandanna_oli","G_Bandanna_shades","G_Bandanna_sport","G_Bandanna_tan"];
				};

		case 34: {
					_toAddWeapons = ["srifle_DMR_03_F","srifle_DMR_03_multicam_F","srifle_DMR_03_khaki_F","srifle_DMR_03_tan_F","srifle_DMR_03_woodland_F"];
					_toAddPacks =	["B_Kitbag_mcamo","B_Kitbag_sgg","B_Kitbag_cbr"];
				};

		case 35: {
					_toAddWeapons = ["srifle_EBR_F","hgun_Pistol_heavy_01_F"];
					_toAddMags =	["20Rnd_762x51_Mag","11Rnd_45ACP_Mag"];
					_toAddItems =	["optic_Arco","optic_MRD","V_PlateCarrier1_rgr","V_PlateCarrierIA1_dgtl"];
				};

		case 36: {
					_toAddItems =	["H_Booniehat_khk","H_Booniehat_indp","H_Booniehat_mcamo","H_Booniehat_grn","H_Booniehat_tan","H_Booniehat_dirty","H_Booniehat_dgtl","H_Booniehat_khk_hs"];
				};

		case 37: {};

		case 38: {
					_toAddItems =	["U_B_GhillieSuit"];
				};

		case 39: {
					_toAddItems =	["optic_KHS_blk","optic_KHS_hex","optic_KHS_tan"];
				};

		case 40: {
					_toAddWeapons = ["LMG_Mk200_F"];
					_toAddMags =	["200Rnd_65x39_cased_Box_Tracer"];
					_toAddItems =	["optic_NVS","U_B_CombatUniform_mcam_vest"];
				};

		case 41: {
					_toAddItems =	["V_PlateCarrierGL_blk","V_PlateCarrierGL_rgr","V_PlateCarrierGL_mtp"];
				};

		case 42: {
					_toAddItems =	["H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB_paint2","H_HelmetSpecB_blk"];
				};

		case 43: {
					_toAddWeapons = ["srifle_DMR_04_F","srifle_DMR_04_Tan_F"];
				};

		case 44: {
					_toAddItems =	["G_Balaclava_blk","G_Balaclava_combat","G_Balaclava_lowprofile","G_Balaclava_oli"];
				};

		case 45: {
					_toAddWeapons = ["LMG_Zafir_F"];
					_toAddMags =	["150Rnd_762x51_Box_Tracer"];
					_toAddItems =	["V_PlateCarrierGL_rgr","V_PlateCarrierIAGL_dgtl"];
					_toAddPacks =	["B_Carryall_ocamo","B_Carryall_oucamo","B_Carryall_mcamo","B_Carryall_oli","B_Carryall_khk","B_Carryall_cbr"];
				};

		case 46: {
					_toAddWeapons = ["hgun_Pistol_heavy_02_F"];
					_toAddMags =	["6Rnd_45ACP_Cylinder"];
					_toAddItems =	["optic_Yorris"];
				};

		case 47: {};

		case 48: {
					_toAddItems =	["H_HelmetB","H_HelmetB_camo","H_HelmetB_paint","H_HelmetB_light"];
				};

		case 49: {};

		case 50: {
					_toAddWeapons = ["srifle_LRR_F"];
					_toAddMags =	["7Rnd_408_Mag"];
					_toAddItems =	["U_B_CTRG_1","U_B_CTRG_2","U_B_CTRG_3"];
				};

		case 51: {};

		case 52: {
					_toAddItems =	["H_HelmetB_plain_mcamo","H_HelmetB_plain_blk"];
				};

		case 53: {
					_toAddItems =	["U_B_FullGhillie_ard"];
				};

		case 54: {};

		case 55: {
					_toAddWeapons = ["launch_RPG32_F"];
					_toAddMags =	["RPG32_F"];
				};

		case 56: {
					_toAddWeapons = ["srifle_DMR_05_blk_F","srifle_DMR_05_hex_F","srifle_DMR_05_tan_f"];
				};

		case 57: {};

		case 58: {
					_toAddItems =	["G_Aviator"];
				};

		case 59: {
					_toAddItems =	["U_B_FullGhillie_sard"];
				};

		case 60: {
					_toAddWeapons = ["srifle_GM6_F"];
					_toAddMags =	["5Rnd_127x108_Mag"];
					_toAddItems =	["optic_LRPS"];
				};

		case 61: {};

		case 62: {
					_toAddItems =	["H_Beret_blk","H_Beret_red","H_Beret_grn","H_Beret_grn_SF","H_Beret_brn_SF","H_Beret_02","H_Beret_Colonel"];
				};

		case 63: {
					_toAddItems =	["U_B_FullGhillie_lsh"];
				};

		case 64: {
					_toAddWeapons = ["srifle_DMR_02_F","srifle_DMR_02_camo_F","srifle_DMR_02_sniper_F"];
				};

		case 65: {
					_toAddWeapons = ["launch_NLAW_F"];
					_toAddMags =	["NLAW_F"];
				};

		case 66: {
					_toAddItems =	["V_PlateCarrierIAGL_dgtl","V_PlateCarrierIAGL_oli"];
				};

		case 67: {
					_toAddItems =	["Laserdesignator"];
				};

		case 68: {
					_toAddItems =	["G_Tactical_Clear","G_Tactical_Black"];
				};

		case 69: {};
		case 70: {
					_toAddItems =	["G_Goggles_VR","optic_Nightstalker"];
				};
	};

	_addonWeapons = _addonWeapons + _toAddWeapons;
	_addonMags = _addonMags + _toAddMags;
	_addonItems = _addonItems + _toAddItems;
	_addonPacks = _addonPacks + _toAddPacks;
};

Arsweapons = _basicWeapons + _addonWeapons;
Arsmagazines = _basicMags + _addonMags;
Arsitems = _basicItems + _addonItems;
Arsbackpacks = _basicPacks + _addonPacks;
