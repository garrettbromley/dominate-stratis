private ["_ammoCratesUsed", "_playerPos", "_chuteType", "_crateType", "_chute"];

_ammoCratesUsed = player getVariable "ammoCratesUsed";
_ammoCratesUsed = _ammoCratesUsed + 1;
player setVariable ["ammoCratesUsed", _ammoCratesUsed, true];

player removeAction ammoCrateReward;
player setVariable ["ammoCrateUsed", true, true];
ammoCrateReward = nil;

player sidechat "Air Supply Drop Incoming...";

sleep 15;

_playerPos = getPos player;
_chuteType = "B_Parachute_02_F";
_crateType =  "B_CargoNet_01_ammo_F";

_chute = createVehicle [_chuteType, [100, 100, 200], [], 0, 'FLY'];
_chute setPos [_playerPos select 0, _playerPos select 1, 200];
_crate = createVehicle [_crateType, position _chute, [], 0, 'NONE'];
_crate attachTo [_chute, [0, 0, -1.3]];

clearWeaponCargoGlobal _crate;
clearMagazineCargoGlobal _crate;
clearItemCargoGlobal _crate;
clearBackpackCargoGlobal _crate;

_crate addMagazineCargoGlobal ["30Rnd_556x45_Stanag_Tracer_Green", 20];		//5.56 30 rnd
_crate addMagazineCargoGlobal ["30Rnd_65x39_caseless_mag_Tracer", 20];		//6.5 30 rnd
_crate addMagazineCargoGlobal ["100Rnd_65x39_caseless_mag_Tracer", 20];		//6.5 100 rnd
_crate addMagazineCargoGlobal ["10Rnd_762x54_Mag", 20];						//7.62 10 rnd
_crate addMagazineCargoGlobal ["20Rnd_762x51_Mag", 20];						//7.62 20 rnd
_crate addMagazineCargoGlobal ["150Rnd_762x54_Box_Tracer", 20];				//7.62 150 rnd
_crate addMagazineCargoGlobal ["200Rnd_65x39_cased_Box_Tracer", 20];		//200 rnd 6.5
_crate addMagazineCargoGlobal ["10Rnd_127x54_Mag", 20];						//12.7 10 rnd
_crate addMagazineCargoGlobal ["5Rnd_127x108_APDS_Mag", 20];				//12.7 5 rnd
_crate addMagazineCargoGlobal ["10Rnd_93x64_DMR_05_Mag", 20];				//9.3 10 rnd
_crate addMagazineCargoGlobal ["150Rnd_93x64_Mag", 20];						//9.3 150 rnd
_crate addMagazineCargoGlobal ["130Rnd_338_Mag", 20];						//.338 130 rnd
_crate addMagazineCargoGlobal ["10Rnd_338_Mag", 20];						//.338 10 rnd
_crate addMagazineCargoGlobal ["7Rnd_408_Mag", 20];							//.408 7 rnd
_crate addMagazineCargoGlobal ["30Rnd_9x21_Mag", 20];						//9 mm 30 rnd
_crate addMagazineCargoGlobal ["30Rnd_45ACP_Mag_SMG_01_tracer_green", 20];	//.45 ACP 30 rnd
_crate addMagazineCargoGlobal ["RPG32_F", 2];								// RPG
_crate addMagazineCargoGlobal ["NLAW_F", 2];								// PCML

_crate setVelocity [0, 0, -25];

waitUntil {position _crate select 2 < 0.5 || isNull _chute};
detach _crate;
_crate setPos [position _crate select 0, position _crate select 1, 0];

sleep 180;

deleteVehicle _crate;
