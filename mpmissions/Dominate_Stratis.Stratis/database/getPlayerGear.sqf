if (!isServer) exitWith {};

private ["_UID", "_playerUnit", "_playerGear", "_playerSide", "_result"];

_UID = _this select 0;
_playerUnit = _this select 1;
_playerSide = _this select 2;
_playerGear = "";

if (_playerSide == "WEST") then {
	_result = [format ["getPlayerGearWEST:%1", _UID], 2] call extDB_fnc_async;
	diag_log format ["getPlayerGearWEST: %1", _result];
};

if (_playerSide == "EAST") then {
	_result = [format ["getPlayerGearEAST:%1", _UID], 2] call extDB_fnc_async;
};

if (count _result > 0) then
{
	_playerGear = _result select 0;
};

_playerUnit setVariable ["playerGear", _playerGear, true];
