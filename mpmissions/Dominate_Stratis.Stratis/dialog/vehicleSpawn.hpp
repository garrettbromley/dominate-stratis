class vehicleShop
{
	idd = 2300;
	name="vehicleShop";
	movingEnabled = 0;
	enableSimulation = 1;
	onLoad = "[] execVM 'scripts\serverFunctions\vehicleConfig.sqf'";

	class controlsBackground
	{
		class RscTitleBackground : RscTextDS
		{
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
			idc = -1;
			x = 0.1;
			y = 0.2;
			w = 0.8;
			h = (1 / 25);
		};

		class MainBackground : RscTextDS
		{
			colorBackground[] = {0,0,0,0.7};
			idc = -1;
			x = 0.1;
			y = 0.2 + (11 / 250);
			w = 0.8;
			h = 0.7 - (22 / 250);
		};

		class Title : RscTitleDS
		{
			idc = 2301;
			text = "Available Force Points: ";
			x = 0.1;
			y = 0.2;
			w = 0.8;
			h = (1 / 25);
		};

		class VehicleTitleBox : RscTextDS
		{
			idc = -1;
			text = "Vehicles";
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
			SizeEx = 0.03300;
			x = 0.11; y = 0.26;
			w = 0.3;
			h = (1 / 25);
		};

		class VehicleInfoHeader : RscTextDS
		{
			idc = 2330;
			text = "Vehicle Information";
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
			SizeEx = 0.03300;
			x = 0.42; y = 0.26;
			w = 0.46;
			h = (1 / 25);
		};

		class CloseBtn : RscButtonMenuDS
		{
			idc = -1;
			text = "CLOSE";
			onButtonClick = "closeDialog 0;";
			x = -0.06 + (6.25 / 40) + (1 / 250 / (safezoneW / safezoneH));
			y = 0.9 - (1 / 25);
			w = (6.25 / 40);
			h = (1 / 25);
		};

		class BuyCar : RscButtonMenuDS
		{
			idc = 2309;
			text = "USE RESOURCES";
			onButtonClick = "[] execVM 'scripts\serverFunctions\vehicleSpawn.sqf'";
			x = 0.545 + (6.25 / 40) + (1 / 250 / (safezoneW / safezoneH));
			y = 0.9 - (1 / 25);
			w = (7.8 / 40);
			h = (1 / 25);
		};
	};

	class controls
	{
		class VehicleList : RscListBoxDS
		{
			idc = 2302;
			text = "";
			sizeEx = 0.04;
			colorBackground[] = {0.1,0.1,0.1,0.9};
			onLBSelChanged = "[_this] execVM 'scripts\serverFunctions\vehicleLblChange.sqf'";

			//Position & height
			x = 0.11; y = 0.302;
			w = 0.303; h = 0.49;
		};

		class vehicleInfomationList : RscStructuredTextDS
		{
			idc = 2303;
			text = "";
			sizeEx = 0.035;

			x = 0.41; y = 0.3;
			w = 0.5; h = 0.5;
		};
	};
};