private ["_gear", "_gearFinal"];

_gear = player getVariable "playerGear";

if (typeName _gear == "STRING") then {
	if (_gear == "") then {
		while {_gear == ""} do {
			_gear = player getVariable "playerGear";
			sleep 1;
		};
	};

	_gearFinal = call compile _gear;
};

if (typeName _gear == "ARRAY") then {
	if (count _gear < 1) then {
		while {count _gear < 1} do {
			_gear = player getVariable "playerGear";
			sleep 1;
		};
	};

	_gearFinal = _gear;
};

player setUnitLoadout _gearFinal;

[] call playerFunctions_fnc_gearConfig;
