waitUntil {!(isNull (findDisplay 46))};
disableSerialization;

_rscLayer = "rankBar" call BIS_fnc_rscLayer;
_rscLayer cutRsc["rankBar", "PLAIN"];
systemChat format["Loading rank info...", _rscLayer];

[] spawn {
	sleep 10;

	_loop = true;
	while {_loop} do
	{
		sleep 2;

		private ["_playerUnit", "_playerRankName", "_playerRankNo", "_playerXP", "_playerKills", "_playerDeaths", "_playerKDR", "_str", "_playerRating", "_playerKillStreak", "_playerForcePoints"];

		_playerFriendlyKills = player getVariable "friendlyKills";
		_playerRankName = player getVariable "playerRankName";
		_playerRankNo = player getVariable "playerRankNo";
		_playerRankIcon = player getVariable "playerRankIcon";
		_calcAccuracy = true;

		if (_playerFriendlyKills >= 3) then {
			["TeamKiller", false, true] call BIS_fnc_endMission;
			_loop = false;
		};

		_playerXP = player getVariable "playerXP";
		_playerXP = [_playerXP] call BIS_fnc_numberText;

		_playerKills = player getVariable "playerKills";
		_playerDeaths = player getVariable "playerDeaths";
		_playerKillStreak = player getVariable "killStreak";
		_playerForcePoints = player getVariable "forcePoints";

		if (_playerKills > _playerDeaths) then {
			if (_playerDeaths == 0) then {
				_playerKDR = _playerKills;
			} else {
				_playerKDR = (_playerKills / _playerDeaths);
				_playerKDR = round (_playerKDR * (10 ^ 2)) / (10 ^ 2);
			};
		};

		if (_playerDeaths > _playerKills) then {
			if (_playerKills == 0) then {
				_playerKDR = _playerDeaths - (_playerDeaths * 2);
			} else {
				_playerKDR = (_playerDeaths / _playerKills);
				_playerKDR = round (_playerKDR * (10 ^ 2)) / (10 ^ 2);
				_playerKDR = _playerKDR - (_playerKDR * 2);
			};
		};

		if (_playerKills == _playerDeaths) then {
			_playerKDR = 1;
		};

		if ((_playerDeaths == 0) && (_playerKills == 0)) then {
			_playerKDR = 0;
		};

		((uiNamespace getVariable "rankBar")displayCtrl 1200)ctrlSetText format["%1", _playerRankIcon];
		((uiNamespace getVariable "rankBar")displayCtrl 1000)ctrlSetText format["%1", _playerRankName];
		((uiNamespace getVariable "rankBar")displayCtrl 1001)ctrlSetText format["RANK: %1 | FORCE POINTS: %2", _playerRankNo, _playerForcePoints];
		((uiNamespace getVariable "rankBar")displayCtrl 1002)ctrlSetText format["XP:      %1", _playerXP];
		((uiNamespace getVariable "rankBar")displayCtrl 1003)ctrlSetText format["KILLS: %1 | DEATHS: %2 | KDR: %3 | STREAK: %4", _playerKills, _playerDeaths, _playerKDR, _playerKillStreak];
	};
};
