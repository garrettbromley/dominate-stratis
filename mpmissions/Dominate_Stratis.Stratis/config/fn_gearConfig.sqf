if (isServer) exitWith {};

switch (playerSide) do {
	case west: { [] execVM "config\westGearConfig.sqf"; };
	case east: { [] execVM "config\eastGearConfig.sqf"; };
};
