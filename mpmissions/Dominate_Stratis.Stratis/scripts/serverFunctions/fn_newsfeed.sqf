waitUntil {!(isNull (findDisplay 46))};
disableSerialization;

_rscLayer = "newsfeed" call BIS_fnc_rscLayer;
_rscLayer cutRsc["newsfeed", "PLAIN"];
systemChat format["Loading newsfeed...", _rscLayer];

[] spawn {
	sleep 15;

	_loop = true;
	while {_loop} do
	{
		sleep 1;

		private ["_playerUnit", "_playerNews", "_newsFeedData", "_curData"];

		_playerNews = player getVariable "playerNews";
		_newsfeedData = "";
		_breakString = "\n";


		_newsLength = count _playerNews;

		if (_newsLength > 0) then {
			for [{_i = 0}, {_i < _newsLength}, {_i = _i + 1}] do
			{
				_curData = (_playerNews select _i) select 0;
				_newsfeedData = _newsfeedData + _curData;
				_newsfeedData = _newsfeedData + _breakString;
			};
		};

		((uiNamespace getVariable "newsfeed")displayCtrl 1100)ctrlSetText format["%1", _newsfeedData];
		((uiNamespace getVariable "newsfeed")displayCtrl 1100)ctrlSetTextColor [1, 1, 1, .5];
	};
};
