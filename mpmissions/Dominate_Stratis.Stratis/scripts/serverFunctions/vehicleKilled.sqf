private ["_killer", "_killersXP", "_killersKills", "_killerNewsFeed", "_newKillersKills", "_xpForKill", "_newXP", "_killFeed", "_killerNewsFeed"];

if (isNull (_this select 1)) then {
	_killer = _this select 0;
} else {
	_killer = _this select 1;
};

_xpForKill = _this select 2;

if !(isPlayer _killer) exitWith {};

_killersXP = _killer getVariable "playerXP";
_killersVehicleKills = _killer getVariable "vehicleKills";
_killerNewsFeed = _killer getVariable "playerNews";

_newXP = _killersXP + _xpForKill;

_newVehicleKills = _killersVehicleKills + 1;

_killFeed = format ["[ %1+ ] You destroyed a vehicle", _xpForKill];
_killerNewsFeed pushBack [_killFeed, 10];

_killer setVariable ["playerXP", _newXP, true];
_killer setVariable ["vehicleKills", _newVehicleKills, true];

_killer setVariable ["playerNews", _killerNewsFeed, true];
["scripts\serverFunctions\activateNewsfeed.sqf","BIS_fnc_execVM", _killer, false, false] call BIS_fnc_MP;

[[], "fn_hudUpdate",true,true] call BIS_fnc_MP;
