A game mode developed for Arma 3 (a military sandbox for the PC) that places 3 factions in a battle for control over an island with a massive amount of features. With 50 players on each side, and over 50 AI at any given time, this 3-pronged battle for control over several points across the island gets intense and is extremely fun when playing with a group of friends.

Some key programming features include:

* Player progression
* Database saving for player progress and gear selections
* Statistic gathering with key gameplay information
* Ability to view leaderboards of gameplay statistics on our website
* Several completely custom user interface dialogs for displaying key information to the player
* Over 27,500 lines of code in the project
* Server/Client optimized code for most effective use of resources (Small Footprint)