if (!isServer) exitWith {};

private ["_UID", "_xp", "_humanKills", "_friendlyKills", "_aiKills", "_vehicleKills", "_longestPlayerKill", "_longestAIKill", "_largestKillStreak", "_deathsFromPlayers", "_deathsFromAI", "_ammoCratesUsed", "_uavsUsed", "_mortarsUsed", "_capturedPoints", "_forcePointsGained", "_forcePointsContributed", "_forcePointsSpent", "_aliveTime"];

_UID = _this select 0;
_xp = _this select 1;
_humanKills = _this select 2;
_friendlyKills = _this select 3;
_aiKills = _this select 4;
_vehicleKills = _this select 5;
_longestPlayerKill = _this select 6;
_longestAIKill = _this select 7;
_largestKillStreak = _this select 8;
_deathsFromPlayers = _this select 9;
_deathsFromAI = _this select 10;
_ammoCratesUsed = _this select 11;
_uavsUsed = _this select 12;
_mortarsUsed = _this select 13;
_capturedPoints = _this select 14;
_forcePointsGained = _this select 15;
_forcePointsContributed = _this select 16;
_forcePointsSpent = _this select 17;
_aliveTime = _this select 18;

[format ["setPlayerData:%1:%2:%3:%4:%5:%6:%7:%8:%9:%10:%11:%12:%13:%14:%15:%16:%17:%18:%19", _UID, _xp, _humanKills, _friendlyKills, _aiKills, _vehicleKills, _longestPlayerKill, _longestAIKill, _largestKillStreak, _deathsFromPlayers, _deathsFromAI, _ammoCratesUsed, _uavsUsed, _mortarsUsed, _capturedPoints, _forcePointsGained, _forcePointsContributed, _forcePointsSpent, _aliveTime], 2] call extDB_fnc_async;
