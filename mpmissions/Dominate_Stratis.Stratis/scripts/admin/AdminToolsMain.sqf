_pathtotools = "scripts\admin\tools\";
_EXECscript1 = 'player execVM "'+_pathtotools+'%1"';

//customise these menus to fit your server
if ((getPlayerUID player) in allAdmins) then { //all admins
	if ((getPlayerUID player) in modList) then { // mods
        adminmenu =
        [
			["",true],
				["Tools Menu", [2], "#USER:ModToolsMenu", -5, [["expression", ""]], "1", "1"],
				["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
        ];};
	if ((getPlayerUID player) in adminList) then { // admins
		adminmenu =
		[
			["",true],
				["Tools Menu", [2], "#USER:AdminToolsMenu", -5, [["expression", ""]], "1", "1"],
				["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
		];};
};

ModToolsMenu =
[
	["Tools",true],
		["Spectate Player", [2],  "", -5, [["expression", format[_EXECscript1,"spectate.sqf"]]], "1", "1"],
		["Teleport To Me", [3], "", -5, [["expression", format[_EXECscript1, "teleportp_to.sqf"]]], "1", "1"],
		["Teleport", [4], "", -5, [["expression", format[_EXECscript1, "teleport.sqf"]]], "1", "1"],
		["Heal Player(s)", [5],  "", -5, [["expression", format[_EXECscript1,"healp.sqf"]]], "1", "1"],
		["Building Repair", [6],  "", -5, [["expression", format[_EXECscript1,"repairbuild.sqf"]]], "1", "1"],
		["", [-1], "", -5, [["expression", ""]], "1", "0"],
			["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];

AdminToolsMenu =
[
	["Tools",true],
	["Spectate Player", [4],  "", -5, [["expression", format[_EXECscript1,"spectate.sqf"]]], "1", "1"],
	["Teleport", [2], "#USER:ToolsMenuTeleport", -5, [["expression", ""]], "1", "1"],
	["Players", [3], "#USER:ToolsMenuPlayers", -5, [["expression", ""]], "1", "1"],
	["Miscellaneous", [4], "#USER:ToolsMenuMiscellaneous", -5, [["expression", ""]], "1", "1"],
	["Super Admin", [5], "#USER:ToolsMenuSuperAdmin", -5, [["expression", ""]], "1", "1"],
	["", [-1], "", -5, [["expression", ""]], "1", "0"],
	["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];
ToolsMenuTeleport =
[
	["Tools - Teleport",true],
        ["Teleport Self", [2],  "", -5, [["expression", format[_EXECscript1,"teleport.sqf"]]], "1", "1"],
        ["Teleport Self to Player", [3],  "", -5, [["expression", format[_EXECscript1,"teleport_to.sqf"]]], "1", "1"],
        ["Teleport Player(s) to Location", [4],  "", -5, [["expression", format[_EXECscript1,"teleportp.sqf"]]], "1", "1"],
        ["Teleport Player(s) to Me", [5],  "", -5, [["expression", format[_EXECscript1,"teleportp_to.sqf"]]], "1", "1"],
	["", [-1], "", -5, [["expression", ""]], "1", "0"],
	["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];
ToolsMenuPlayers =
[
	["Tools - Players",true],
	["Heal Self", [2],  "", -5, [["expression", format[_EXECscript1,"heal.sqf"]]], "1", "1"],
	["Heal Player(s)", [3],  "", -5, [["expression", format[_EXECscript1,"healp.sqf"]]], "1", "1"],
	["", [-1], "", -5, [["expression", ""]], "1", "0"],
	["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];
ToolsMenuMiscellaneous =
[
	["Tools - Miscellaneous",true],
	["Building Repair", [2],  "", -5, [["expression", format[_EXECscript1,"repairbuild.sqf"]]], "1", "1"],
	["Delete Vehicle", [3],  "", -5, [["expression", format[_EXECscript1,"veh_delete.sqf"]]], "1", "1"],
	["Repair Vehicle(s)", [4], "", -5, [["expression", format[_EXECscript1,"repairflip.sqf"]]], "1", "1"],
	["Player Markers On", [5],  "", -5, [["expression", format[_EXECscript1,"adminMarkers.sqf"]]], "1", "1"],
	["Player Markers Off", [6],  "", -5, [["expression", format[_EXECscript1,"adminMarkersOff.sqf"]]], "1", "1"],
	["", [-1], "", -5, [["expression", ""]], "1", "0"],
	["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];
ToolsMenuSuperAdmin =
[
	["Tools - Super Admin",true],
	["God Mode On", [2],  "", -5, [["expression", format[_EXECscript1,"godOn.sqf"]]], "1", "1"],
	["God Mode Off", [3],  "", -5, [["expression", format[_EXECscript1,"godOff.sqf"]]], "1", "1"],
	["Vehicle God", [4],  "", -5, [["expression", format[_EXECscript1,"veh_god.sqf"]]], "1", "1"],
	["Kill Player(s)", [5],  "", -5, [["expression", format[_EXECscript1,"killp.sqf"]]], "1", "1"],
	["", [-1], "", -5, [["expression", ""]], "1", "0"],
	["Exit", [13], "", -3, [["expression", ""]], "1", "1"]
];
showCommandingMenu "#USER:adminmenu";
