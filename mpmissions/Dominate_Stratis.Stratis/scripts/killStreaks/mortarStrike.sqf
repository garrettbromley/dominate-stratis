private ["_vehicle", "_veh", "_sp", "_mortarsUsed"];

_mortarsUsed = player getVariable "mortarsUsed";
_mortarsUsed = _mortarsUsed + 1;
player setVariable ["mortarsUsed", _mortarsUsed, true];

player removeAction mortarStrikeReward;
player setVariable ["mortarStrikeUsed", true, true];
mortarStrikeReward = nil;

player sidechat "Setting up your Mortar...";
player playMove "AinvPknlMstpSnonWnonDnon_medic_1";
sleep 3;

_sp = [(getpos player select 0) + 0, (getpos player select 1) + 0, getpos player select 2];

switch (side player) do {
	case west: {_veh = "B_Mortar_01_F";};
	case east: {_veh = "O_Mortar_01_F";};
};

_vehicle = _veh createVehicle _sp;

player sideChat "Mortar Deployed...";

waitUntil {(!alive _vehicle)};

sleep 10;

deleteVehicle _vehicle;
