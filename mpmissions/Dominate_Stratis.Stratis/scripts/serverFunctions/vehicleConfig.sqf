fnc_fetchVehInfo = {
	private["_class", "_scope", "_picture", "_displayName", "_vehicleClass", "_side", "_faction", "_superClass", "_speed", "_armor", "_seats", "_hp", "_fuel"];

	_class = [_this,0,"",[""]] call BIS_fnc_param;

	if(_class == "") exitWith {[]};
	if(!isClass (configFile >> "CfgVehicles" >> _class)) exitWith {[]};

	_scope = -1;
	_picture = "";
	_displayName = "";
	_vehicleClass = "";
	_side = -1;
	_faction = "";
	_speed = 0;
	_armor = 0;
	_seats = 0;
	_hp = 0;
	_fuel = 0;

	_scope = getNumber(configFile >> "CfgVehicles" >> _class >> "scope");
	_picture = getText(configFile >> "CfgVehicles" >> _class >> "picture");
	_displayName = getText(configFile >> "CfgVehicles" >> _class >> "displayName");
	_vehicleClass = getText(configFile >> "CfgVehicles" >> _class >> "vehicleClass");
	_side = getNumber(configFile >> "CfgVehicles" >> _class >> "side");
	_faction = getText(configFile >> "CfgVehicles" >> _class >> "faction");
	_superClass = configName(inheritsFrom (configFile >> "CfgVehicles" >> _class));
	_speed = getNumber(configFile >> "CfgVehicles" >> _class >> "maxSpeed");
	_armor = getNumber(configFile >> "CfgVehicles" >> _class >> "armor");
	_seats = getNumber(configFile >> "CfgVehicles" >> _class >> "transportSoldier");
	_hp = getNumber(configFile >> "CfgVehicles" >> _class >> "enginePower");
	_fuel = getNumber(configFile >> "CfgVehicles" >> _class >> "fuelCapacity");

	[_class,_scope,_picture,_displayName,_vehicleClass,_side,_faction,_superClass,_speed,_armor,_seats,_hp,_fuel];
};

switch (side player) do {
	case WEST: {
		vehicleList = [
			["B_Heli_Light_01_F",50],
			["B_Heli_Light_01_armed_F",250],
			["B_Heli_Attack_01_F",500],
			["B_Heli_Transport_01_F",150],
			["B_MBT_01_cannon_F",500],
			["B_APC_Wheeled_01_cannon_F",250],
			["B_MRAP_01_F",75],
			["B_MRAP_01_gmg_F",200],
			["B_MRAP_01_hmg_F",150],
			["B_G_Offroad_01_F",25],
			["B_G_Offroad_01_armed_F",75]
		];
	};

	case EAST: {
		vehicleList =	[
			["B_Heli_Light_01_F",50],
			["O_Heli_Light_02_F",250],
			["O_Heli_Attack_02_F",500],
			["O_Heli_Light_02_unarmed_F",150],
			["O_MBT_02_cannon_F",500],
			["O_APC_Tracked_02_cannon_F",250],
			["O_MRAP_02_F",75],
			["O_MRAP_02_gmg_F",200],
			["O_MRAP_02_hmg_F",150],
			["O_G_Offroad_01_F",25],
			["O_G_Offroad_01_armed_F",75]
		];
	};
};

disableSerialization;
_control = ((findDisplay 2300) displayCtrl 2302);
lbClear _control;
ctrlShow [2330,false];
ctrlShow [2304,false];

{
	_className = _x select 0;
	_basePrice = _x select 1;

	_vehicleInfo = [_className] call fnc_fetchVehInfo;

	_control lbAdd (_vehicleInfo select 3);
	_control lbSetPicture [(lbSize _control)-1,(_vehicleInfo select 2)];
	_control lbSetData [(lbSize _control)-1,_className];
	_control lbSetValue [(lbSize _control)-1,_ForEachIndex];
} foreach vehicleList;
